using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using Ants;

namespace GatherBattles
{
	public class BattleAnt : IBattleAnt
	{
		// { from: [0, 1], to: [0, 2], dirs: 'senw', die: true }
		public BattleAnt(dynamic antData, int playerIndex)
			: this(
			new Ant(antData["from"][0], antData["from"][1], playerIndex),
			new Ant(antData["to"][0], antData["to"][1], playerIndex), 
			((string)antData["dirs"]).ToCharArray(), 
			(bool)antData["die"])
		{
		}

		public BattleAnt(Ant from, Ant to, char[] allowedDirs, bool die)
		{
			Die = die;
			From = from;
			To = to;
			AllowedDirs = allowedDirs;
			Dirs = AllowedDirs.Select(d => GatherBattles.Dirs.ToDir[d]).ToArray();
		}

		public override string ToString()
		{
			return string.Format("{{ from: [{0},{1}], to: [{2},{3}], dirs: '{4}', die: {5}}}", From.Row, From.Col, To.Row, To.Col, new string(AllowedDirs), Die.ToString().ToLower());
		}

		public Ant From { get; private set; }

		public Ant Ant
		{
			get { return From; }
		}

		public char[] AllowedDirs { get; private set; }
		public Direction[] Dirs { get; private set; }
		public readonly Ant To;
		public readonly bool Die;

		public BattleAnt(AntHistory antHistory, IGameState state)
			: this(antHistory.Ant, antHistory.GetNextLoc(state.TurnNo, state.Height, state.Width), GetAllowedDirs(state, antHistory.Ant), antHistory.End == state.TurnNo + 1)
		{
		}

		private static char[] GetAllowedDirs(IGameState state, Loc ant)
		{
			return Ants.Ants.Aim.Keys.Where(dir => state.IsLand(ant, dir)).Select(dir => dir.ToChar()).ToArray();
		}
	}

	public class Battle
	{
		public int Turn;
		public readonly int Game;
		public readonly int Rows;
		public readonly int Cols;
		public readonly BattleAnt[][] Ants;

		public Battle(string jsonData)
			: this(new JavaScriptSerializer().DeserializeObject(jsonData))
		{
		}

		// { game: 'sdfsdf', size: [100, 100], ants: [
		//   [{}, {}], 
		//   [{}]
		// ]
		public Battle(dynamic battleData)
		{
			Turn = battleData["turn"];
			Game = battleData["game"];
			Rows = battleData["size"][0];
			Cols = battleData["size"][1];
			var res = new List<BattleAnt[]>();
			foreach (var playerAnts in battleData["ants"])
			{
				var playerBattleAnts = new List<BattleAnt>();
				for (int iPlayer = 0; iPlayer < playerAnts.Length; iPlayer++)
					playerBattleAnts.Add(new BattleAnt(playerAnts[iPlayer], iPlayer));
				res.Add(playerBattleAnts.ToArray());
			}
			Ants = res.ToArray();
		}

		public Battle(int game, int turn, int rows, int cols, BattleAnt[][] ants)
		{
			Game = game;
			Turn = turn;
			Rows = rows;
			Cols = cols;
			Ants = ants;
		}

		public int AntsCount()
		{
			return Ants.Sum(playerAnts => playerAnts.Length);
		}

		public override string ToString()
		{
			var s = new StringBuilder();
			s.AppendFormat("{{game:{0}, turn:{1}, size:[{2},{3}], antsCount: {4}, ants: [ \r\n  ", Game, Turn, Rows, Cols, Ants.Sum(pl => pl.Count()));
			var body = Ants.Select(player => "[" + player.Select(ant => ant.ToString()).JoinStrings(", ") + "]").JoinStrings(",\r\n  ");
			s.Append(body);
			s.Append("]}");
			return s.ToString();
		}
	}
}