using Ants;

namespace GatherBattles
{
	public static class Dirs
	{
		public static readonly Loc[] ToLoc;
		public static readonly Direction[] ToDir;

		static Dirs()
		{
			ToLoc = new Loc[255];
			ToLoc['-'] = new Loc(0, 0);
			ToLoc['n'] = new Loc(-1, 0);
			ToLoc['s'] = new Loc(1, 0);
			ToLoc['e'] = new Loc(0, 1);
			ToLoc['w'] = new Loc(0, -1);
			ToDir = new Direction[255];
			ToDir['-'] = Direction.None;
			ToDir['n'] = Direction.North;
			ToDir['s'] = Direction.South;
			ToDir['e'] = Direction.East;
			ToDir['w'] = Direction.West;
		}
		
	}
	public class AntHistory
	{
		public int Col;
		public char[] Directions;
		public int End, PlayerIndex;
		public int Row;
		public int Start;


		public AntHistory(dynamic antData)
		{
			Row = antData[0];
			Col = antData[1];
			Start = antData[2];
			End = antData[3];
			PlayerIndex = antData[4];
			Directions = antData[5].ToCharArray();
		}

		public Ant Ant
		{
			get { return new Ant(Row, Col, PlayerIndex); }
		}

		public bool ApplyMove(int turn, int rows, int cols)
		{
			if (turn - Start - 1 >= Directions.Length) return false;
			char dir = Directions[turn - Start - 1];
			Loc delta = Dirs.ToLoc[dir];
			Row = (Row + delta.Row + rows)%rows;
			Col = (Col + delta.Col + cols)%cols;
			return true;
		}

		public Ant GetNextLoc(int turn, int rows, int cols)
		{
			char dir = Directions[turn - Start];
			Loc delta = Dirs.ToLoc[dir];
			return new Ant((Row + delta.Row + rows) % rows, (Col + delta.Col + cols) % cols, PlayerIndex);
		}

		public bool IsAlive(int turnNo)
		{
			return turnNo >= Start && turnNo < End;
		}

		public override string ToString()
		{
			return string.Format("Col: {0}, Row: {1}, PlayerIndex: {2}", Col, Row, PlayerIndex);
		}
	}
}