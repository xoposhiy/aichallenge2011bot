﻿using System;
using System.IO;
using System.Linq;
using System.Web.Script.Serialization;
using Ants;

namespace GatherBattles
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			var uniq = new HashSet<int>();
			string pattern = args.Any() ? args[0] : "*.json";
			var files = Directory.GetFiles(".", pattern);
			foreach (var file in files)
			{
				var battles = new BattleEnumerator(Replay.Load(file)).GetBattles();
				foreach (var b in battles)
				{
					if (b.AntsCount() <= 2) continue;
					var s = b.ToString();
					b.Turn = 0;
					var hashCode = b.ToString().GetHashCode();
					if (!uniq.Contains(hashCode))
					{
						uniq.Add(hashCode);
						Console.WriteLine(s);
					}
//					Тестирование сериализации:
//					var s2 = new Battle(new JavaScriptSerializer().DeserializeObject(s)).ToString();
//					Console.Error.WriteLine(s2);
				}
			}
		}

	}

	class Replay
	{
		public dynamic Data;

		private Replay(object data)
		{
			this.Data = data;
		}

		public static Replay Load(string filename)
		{
			dynamic data = new JavaScriptSerializer().DeserializeObject(File.ReadAllText(filename));
			return new Replay(data);
		}
	}
}