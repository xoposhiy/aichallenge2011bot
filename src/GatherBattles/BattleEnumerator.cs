using System.Collections.Generic;
using System.Linq;
using Ants;

namespace GatherBattles
{
	internal class BattleEnumerator
	{
		private readonly Replay replay;
		private readonly int rows;
		private readonly int cols;
		private readonly List<AntHistory> ants;
		private readonly GameState state;
		private readonly int lastTurn;
		private readonly int gameId;

		public BattleEnumerator(Replay replay)
		{
			this.replay = replay;
			dynamic replaydata = replay.Data["replaydata"];

			ants = new List<AntHistory>();
			foreach (dynamic ant in replaydata["ants"])
				ants.Add(new AntHistory(ant));
			lastTurn = replay.Data["game_length"];
			dynamic map = replaydata["map"];
			rows = map["rows"];
			cols = map["cols"];
			gameId = replay.Data["game_id"];
			state = new GameState(cols, rows, 0, 0, 77, 5, 1, 0, true);
			LoadMap(map["data"]);
		}

		private void LoadMap(dynamic data)
		{
			for(int r=0; r<data.Length; r++)
				for(int c=0; c<data[r].Length; c++)
					if (data[r][c] == '%') state.AddWater(r, c);
		}

		private IEnumerable<Battle> SearchBattles()
		{
			var markedAnts = new Ants.HashSet<AntHistory>();
			var aliveAnts = ants.Where(a => a.IsAlive(state.TurnNo)).ToList();
			foreach (AntHistory ant in aliveAnts)
			{
				if (!markedAnts.Contains(ant))
				{
					var battle = SearchBattleWith(ant, markedAnts, aliveAnts);
					if (battle != null) yield return battle;
				}
			}
		}

		private static IEnumerable<AntHistory> GetAll(IEnumerable<List<AntHistory>> playersAnts)
		{
			return playersAnts.SelectMany(playerAnts => playerAnts);
		}

		private Battle SearchBattleWith(AntHistory firstAnt, Ants.HashSet<AntHistory> markedAnts, List<AntHistory> aliveAnts)
		{
			var res = new Dictionary<int, List<AntHistory>>();
			var q = new Queue<AntHistory>();
			q.Enqueue(firstAnt);
			res.Add(firstAnt.PlayerIndex, new List<AntHistory> { firstAnt });
			markedAnts.Add(firstAnt);
			while (q.Any())
			{
				AntHistory ant = q.Dequeue();
				IEnumerable<AntHistory> enemies = aliveAnts
					.Where(a => a.PlayerIndex != ant.PlayerIndex)
					.Where(a => !markedAnts.Contains(a))
					.Where(a => state.IsInAttackRangeOnNextTurn(ant.Ant, a.Ant));
				foreach (AntHistory enemy in enemies)
				{
					if (!res.ContainsKey(enemy.PlayerIndex)) res.Add(enemy.PlayerIndex, new List<AntHistory>());
					res[enemy.PlayerIndex].Add(enemy);
					q.Enqueue(enemy);
					markedAnts.Add(enemy);
				}
			}
			if (res.Count > 1)
			{
				var battleAnts = res.Values.Select(player => player.Select(ant => new BattleAnt(ant, state)).ToArray()).ToArray();
				return new Battle(gameId, state.TurnNo, rows, cols, battleAnts);
			}
			return null;
		}

		public IEnumerable<Battle> GetBattles()
		{
			int maxPlayerIndex = ants.Max(a => a.PlayerIndex);
			var aliveAnts = new List<AntHistory>[maxPlayerIndex + 1];
			for (int i = 0; i < maxPlayerIndex + 1; i++)
				aliveAnts[i] = new List<AntHistory>();
			state.TurnNo = 0;
			foreach (AntHistory ant in ants)
			{
				if (ant.Start == state.TurnNo)
					aliveAnts[ant.PlayerIndex].Add(ant);
			}
			state.TurnNo = 1;
			for (int i = 1; i <= lastTurn-1; i++)
			{
				foreach (var playersAnts in aliveAnts)
				{
					foreach (AntHistory ant in playersAnts.ToArray())
					{
						if (!ant.ApplyMove(state.TurnNo, rows, cols))
							playersAnts.Remove(ant);
					}
				}

				foreach (var battle in SearchBattles())
					yield return battle;

				foreach (AntHistory ant in ants)
				{
					if (ant.Start == state.TurnNo)
						aliveAnts[ant.PlayerIndex].Add(ant);
				}
				state.TurnNo++;
			}
		}
	}
}