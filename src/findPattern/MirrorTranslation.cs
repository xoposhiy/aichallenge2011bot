﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ants;

namespace FindPattern
{
    public class MinusMirror : TranslationWithZero
    {
        public MinusMirror(int size, int zeroY) : base(size, new Loc(zeroY != 0 ? zeroY : size, 0)) { }

        public override Ants.Loc Translate(Loc origin)
        {
            var y = (origin.Row - zero.Row + size) % size;
            return new Loc(origin.Row - y + (size - 1 - y), origin.Col);
        }

        public override Loc GetSizeRestriction()
        {
            return new Loc(size, 0);
        }
    }

    public class lMirror : TranslationWithZero
    {
        public lMirror(int size, int zeroX) : base(size, new Loc(0, zeroX != 0 ? zeroX : size)) { }

        public override Ants.Loc Translate(Loc origin)
        {
            var x = (origin.Col - zero.Col + size) % size;
            return new Loc(origin.Row, origin.Col - x + (size - 1 - x));
        }

        public override Loc GetSizeRestriction()
        {
            return new Loc(0, size);
        }
    }

    public class SlashMirror : TranslationWithZero
    {
        public SlashMirror(int size, Loc zero) : base(size, zero) { }

        public override Ants.Loc Translate(Loc origin)
        {
            var x = (origin.Col - zero.Col + size) % size;
            var y = (origin.Row - zero.Row + size) % size;
            return new Loc(origin.Row - y + (size - 1 - x), origin.Col - x + (size - 1 - y));
        }

        public override Loc GetSizeRestriction()
        {
            return new Loc(size, size); //size = width = height
        }
    }

    public class BackslashMirror : TranslationWithZero
    {
        public BackslashMirror(int size, Loc zero) : base(size, zero) { }

        public override Ants.Loc Translate(Loc origin)
        {
            var x = (origin.Col - zero.Col + size) % size;
            var y = (origin.Row - zero.Row + size) % size;
            return new Loc(origin.Row - y + x, origin.Col - x + y);
        }

        public override Loc GetSizeRestriction()
        {
            return new Loc(size, size); //size = width = height
        }
    }
}
