﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;
using Case = System.Tuple<FindPattern.Translation, FindPattern.Translation>;
using Ants;
using System.IO;
using System.Diagnostics;

namespace FindPattern
{
	public partial class Form1 : Form
	{
		private Map map;
		private TranslationFinder finder;
        int scale = 4;
        List<string> mapNames = new List<string>();
        List<string> maskNames = new List<string>();
        int currentMapIndex = 0;
        int currentMaskIndex = 0;
        Color[] colors = new[] { Color.Red, Color.Blue, Color.Yellow, Color.Green, Color.Orange, Color.Violet, Color.LightGreen, Color.LightBlue, Color.Pink, Color.Brown, Color.Fuchsia };
        Form2 form2;

		public Form1(string[] args)
		{
			InitializeComponent();

            LoadMapNames(@"..\..\..\..\tools\maps");
            maskNames.AddRange(Directory.GetFiles(@"..\..", "*.png"));
            ProcessMap(mapNames[0], maskNames[0]);
            form2 = new Form2();
            form2.Show();
		}

        private void LoadMapNames(string root)
        {
            mapNames.AddRange(Directory.GetFiles(root, "*.map"));

            foreach (var childDir in Directory.GetDirectories(root))
                LoadMapNames(childDir);
        }

        public void DrawResult(TranslationPair result, Color color, int mapWidth, int mapHeight)
        {
            var zero1 = result.Item1 == null ? new Loc(0, 0) : result.Item1.GetZeroRestriction();
            var zero2 = result.Item2 == null ? new Loc(0, 0) : result.Item2.GetZeroRestriction();

            var zero = new Loc(Math.Max(zero1.Row, zero2.Row) % result.Size.Row, Math.Max(zero1.Col, zero2.Col) % result.Size.Col);

            var graphics = this.CreateGraphics();
            Pen pen = new Pen(color);

            Loc origin = zero;
            graphics.DrawRectangle(pen, new Rectangle(origin.Col * scale, origin.Row * scale, result.Size.Col * scale, result.Size.Row * scale));
                        
            if (result.Item1 != null)
            {
                origin = result.Item1.Translate(origin);
                graphics.DrawRectangle(pen, new Rectangle(origin.Col * scale, origin.Row * scale, result.Size.Col * scale, result.Size.Row * scale));
            }

            if (result.Item2 != null)
            {
                origin = result.Item2.Translate(origin);
                graphics.DrawRectangle(pen, new Rectangle(origin.Col * scale, origin.Row * scale, result.Size.Col * scale, result.Size.Row * scale));
            }
        }

		public void DrawMap(Map map)
		{

			//var bitmap = new Bitmap(map.Width * 2, map.Height * 2);			
			var graphics = this.CreateGraphics();
			Brush landBrush = new SolidBrush(Color.White);
			Brush waterBrush = new SolidBrush(Color.CadetBlue);
            Brush waterUnseenBrush = new SolidBrush(Color.DarkBlue);
			graphics.Clear(Color.Gray);			

			for(int i = 0; i < map.Width; i ++)
                for (int j = 0; j < map.Height; j++)                
                {
                    Loc loc = new Loc(j, i);
                    if (map[loc] == Ants.Tile.Water || map.Known.Contains(loc))
                    {
                        Brush brush;
                        if (!map.Known.Contains(loc))
                            brush = waterUnseenBrush;
                        else
                            brush = map[i, j] == Ants.Tile.Water ? waterBrush : landBrush;
                        graphics.FillRectangle(brush, new Rectangle(i * scale, j * scale, scale, scale));				
                    }                    
                }
		}		

		private void Form1_Paint(object sender, PaintEventArgs e)
		{
			DrawMap(map);
		}

        private void ProcessMap(string filename, string maskname)
        {
            var start = DateTime.Now;
            map = new Map(filename, maskname);
            TranslationFinder finder = new TranslationFinder(map.Width, map.Height, new TranslationChecker(map, 100));

            var task = System.Threading.Tasks.Task.Factory.StartNew(delegate()
            {
                while (!finder.SearchCompleted)
                {
                    finder.Search(300);

                    this.Invoke((Action)delegate()
                    {
                        this.Text = String.Format("{0} checked. {1} found.", finder.CheckedCount, finder.Results.Count);
                    });
                }


                this.Invoke((Action)delegate()
                {
                    if (finder.Results.Count > 0)
                    {                        
                        form2.ShowResult(String.Join("\r\n", finder.Results.Select(result => result.ToString())));

                        foreach (var res in finder.Results.Take(colors.Length).Select((result, i) => new { translations = result, color = colors[i] }))
                            DrawResult(res.translations, res.color, map.Width, map.Height);
                    }
                    else
                        this.Text = this.Text + " Not found";
                        //MessageBox.Show("not found");

                   this.Text = String.Concat(this.Text, " (", (DateTime.Now - start).TotalMilliseconds.ToString("0"), "ms)");                    
                });
            });
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            lblNote.Visible = false;

            switch(e.KeyCode)
            {
                case Keys.Right:
                    currentMapIndex = Math.Min(currentMapIndex + 1, mapNames.Count - 1);
                    ProcessMap(mapNames[currentMapIndex], maskNames[currentMaskIndex]);
                    break;
                case Keys.Left:
                    currentMapIndex = Math.Max(currentMapIndex - 1, 0);
                    ProcessMap(mapNames[currentMapIndex], maskNames[currentMaskIndex]);
                    break;
                case Keys.Up:
                    currentMaskIndex = Math.Max(currentMaskIndex - 1, 0);
                    ProcessMap(mapNames[currentMapIndex], maskNames[currentMaskIndex]);
                    break;
                case Keys.Down:
                    currentMaskIndex = Math.Min(currentMaskIndex + 1, maskNames.Count - 1);
                    ProcessMap(mapNames[currentMapIndex], maskNames[currentMaskIndex]);
                    break;                    
            }

            this.Invalidate();
        }

       

		
	}
}
