﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ants;

namespace FindPattern
{   
    public abstract class Translation
	{
        public abstract Loc Translate(Loc origin);        
        public abstract Loc GetSizeRestriction();
        public virtual Loc GetZeroRestriction() 
        {
            return new Loc(0, 0);
        }

		public Translation Combine(Translation other)
		{
            return other == null ? this : new CompositeTranslation(this, other);
		}

        public override string ToString()
        {
            return String.Concat(GetType().Name);
        }	
	}

    public abstract class TranslationWithSize : Translation
    { 
        protected int size;
        public TranslationWithSize(int size)
        {
            this.size = size;
        }

        public override string ToString()
        {
            return String.Concat(base.ToString(), " size=", size.ToString());
        }
    }

    public abstract class TranslationWithZero : TranslationWithSize
    {
        protected Loc zero;

        public TranslationWithZero(int size, Loc zero)
            : base(size)
        {
            this.zero = zero;
        }

        public override Loc GetZeroRestriction()
        {
            return zero;
        }

        public override string ToString()
        {
            return String.Concat(base.ToString(), ";zero=", zero.ToString());
        }
    }
}
