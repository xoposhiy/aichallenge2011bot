﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ants;

namespace FindPattern
{
    public class CompositeTranslation : Translation
    {
        private Translation first;
        private Translation second;

        public CompositeTranslation(Translation first, Translation second)
        {
            this.first = first;
            this.second = second;
        }
        
        public override Loc GetSizeRestriction()
        {
            var firstRestriction = first.GetSizeRestriction();
            var secondRestriction = second.GetSizeRestriction();
            return new Loc(Math.Max(firstRestriction.Row, secondRestriction.Row), Math.Max(firstRestriction.Col, secondRestriction.Col));            
        }

        public override Loc GetZeroRestriction()
        {
            var firstRestriction = first.GetZeroRestriction();
            var secondRestriction = second.GetZeroRestriction();
            return new Loc(Math.Max(firstRestriction.Row, secondRestriction.Row), Math.Max(firstRestriction.Col, secondRestriction.Col));
        }

        public override Loc Translate(Loc origin)
        {
            return first.Translate(second.Translate(origin));
        }

        public override string ToString()
        {
            return String.Concat(first.ToString(), " | ", second.ToString());
        }
    }
}
