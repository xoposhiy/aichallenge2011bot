﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Ants;

namespace FindPattern
{
	public class TranslationFinder
	{
        ITranslationChecker checker;
        IEnumerator<TranslationPair> casesEnumerator;
        public ICollection<TranslationPair> Results { get; private set; }
        private int bestRate = int.MaxValue;
		public int CheckedCount { get; private set; }
		public bool SearchCompleted { get; private set; }
        
        public TranslationFinder(int mapWidth, int mapHeigth, ITranslationChecker checker)
		{			
			this.checker = checker;
            casesEnumerator = this.GetTranslations(checker, mapWidth, mapHeigth).GetEnumerator();
            Results = new List<TranslationPair>();
		}

		public int Search(int timeout)
		{
			var start = DateTime.Now;

			do
			{
				SearchCompleted = !casesEnumerator.MoveNext();
				
                if(casesEnumerator.Current != null)                                    
                    Results.Add(casesEnumerator.Current);

                CheckedCount++;
			}
			while (!SearchCompleted && (DateTime.Now - start).TotalMilliseconds < timeout);

			return Results.Count;
		}

        private IEnumerable<TranslationPair> GetTranslations(ITranslationChecker checker, int mapWidth, int mapHeigth)
		{
            var widthDivisors = new List<int> { mapWidth };

            for (int divisor = 3; divisor <= mapWidth / 2; divisor++)
                if (mapWidth % divisor == 0)
                    widthDivisors.Add(divisor);

            var heightDivisors = new List<int> { mapHeigth };

            for (int divisor = 3; divisor <= mapHeigth / 2; divisor++)
                if (mapHeigth % divisor == 0)
                    heightDivisors.Add(divisor);

            foreach (var firstTranslation in GetFirstLevelTranslations(mapWidth, widthDivisors, heightDivisors))
            {
                if (firstTranslation == null || checker.IsTranslationValid(firstTranslation))
                {
                    Loc sizeRestriction = new Loc(0, mapWidth);
                    Loc zeroRestriction = new Loc(0, 0);
                    if (firstTranslation != null)
                    {
                        yield return new TranslationPair(firstTranslation, null, mapWidth, mapHeigth);
                        sizeRestriction = firstTranslation.GetSizeRestriction();
                        zeroRestriction = firstTranslation.GetZeroRestriction();
                    }

                    foreach (var secondTranslation in GetSecondLevelTranslations(mapHeigth, sizeRestriction.Col, sizeRestriction.Row == 0 ? heightDivisors : (IEnumerable<int>)new[] { sizeRestriction.Row }, zeroRestriction))
                    {                        
                        var pair = new TranslationPair(firstTranslation, secondTranslation, mapWidth, mapHeigth);

                        if (pair.Rate < bestRate && checker.IsTranslationValid(secondTranslation))
                        {
                            bestRate = pair.Rate;
                            yield return pair;
                        }
                        else
                            yield return null;
                    }
                }
                else
                    yield return null;
            }
		}

        private IEnumerable<Translation> GetFirstLevelTranslations(int mapWidth, IEnumerable<int> widths, IEnumerable<int> heights)
        {
            foreach (var tileWidth in widths)                
            {
                Translation tile = null;

                //translations using width only                
                if (tileWidth != mapWidth) //if there is horisintal tiling                
                    tile = new HorizontalShift(tileWidth);

                yield return tile;

                for (int zero = 0; zero < tileWidth; zero++)
                {
                    //translations sensetive to horisontal movement of origin
                    yield return new lMirror(tileWidth, zero).Combine(tile);
                }

                var tileHeidth = tileWidth;
                for (int zeroLine = 0; zeroLine < tileWidth + tileHeidth; zeroLine++) //diagonal iteration to gather little zero shifts first
                {
                    Loc zero = new Loc(zeroLine < tileWidth ? 0 : zeroLine - tileWidth + 1, zeroLine < tileWidth ? zeroLine : tileWidth - 1);
                    while (zero.Row < tileHeidth && zero.Col >= 0)
                    {
                        //translations using width=height                
                        yield return new Rotation90(tileWidth, zero).Combine(tile);
                        yield return new Rotation270(tileWidth, zero).Combine(tile);
                        yield return new SlashMirror(tileWidth, zero).Combine(tile);
                        yield return new BackslashMirror(tileWidth, zero).Combine(tile); //slash mirror        
                        
                        zero = new Loc(zero.Col + 1, zero.Row - 1);
                    }
                }                
                

                //translations using both width and height
                foreach (var tileHeight in heights)
                {
                    for (int zero = 0; zero < tileHeight; zero++)
                    {
                        yield return new MinusMirror(tileHeight, zero).Combine(tile); 
                    }
                    
                    for (int zeroLine = 0; zeroLine < tileWidth + tileHeidth; zeroLine++) //diagonal iteration to gather little zero shifts first
                    {
                        Loc zero = new Loc(zeroLine < tileWidth ? 0 : zeroLine - tileWidth + 1, zeroLine < tileWidth ? zeroLine : tileWidth - 1);
                        while (zero.Row < tileHeidth && zero.Col >= 0)
                        {
                            yield return new Rotation180(tileWidth, tileHeight, zero).Combine(tile);
                            zero = new Loc(zero.Col + 1, zero.Row - 1);                            
                        }
                    }
                }
            }
        }

        private IEnumerable<Translation> GetSecondLevelTranslations(int mapHeigth, int tileWidth, IEnumerable<int> heights, Loc zeroRestriction)
        {
            foreach (var tileHeight in heights)
            {
                int verticalRepetitions = mapHeigth / tileHeight;
                var tile = new VerticalShift(tileHeight);
                
                for(int shiftAmount = 0; shiftAmount < tileWidth; shiftAmount++)
                    if ((shiftAmount * verticalRepetitions) % tileWidth == 0)
                    {
                        var shift = shiftAmount == 0 ? null : new HorizontalShift(shiftAmount);
                        Translation tileShift = null;

                        //translation using height only
                        if (tileHeight != mapHeigth) //if there is vertical tiling
                        {
                            tileShift = tile.Combine(shift);
                            yield return tileShift;                            
                        }

                        for (int zeroY = zeroRestriction.Row; zeroY <= (zeroRestriction.Row != 0 ? zeroRestriction.Row : (tileHeight - 1)); zeroY++)
                        {
                            yield return new MinusMirror(tileHeight, zeroY).Combine(tileShift);

                            for (int zeroX = zeroRestriction.Col; zeroX <= (zeroRestriction.Col != 0 ? zeroRestriction.Col : (tileWidth - 1)); zeroX++)
                            {
                                var zero = new Loc(zeroY, zeroX);

                                if (tileHeight == tileWidth)
                                {
                                    //translations using width=height
                                    yield return new Rotation90(tileHeight, zero).Combine(tileShift);
                                    yield return new Rotation270(tileHeight, zero).Combine(tileShift);
                                    yield return new SlashMirror(tileHeight, zero).Combine(tileShift);
                                    yield return new BackslashMirror(tileHeight, zero).Combine(tileShift);
                                }

                                //translations using both width and height
                                yield return new Rotation180(tileWidth, tileHeight, zero).Combine(tileShift);
                            }
                        }

                        for (int zeroX = zeroRestriction.Col; zeroX <= (zeroRestriction.Col != 0 ? zeroRestriction.Col : (tileWidth - 1)); zeroX++)
                            yield return new lMirror(tileWidth, zeroX).Combine(tileShift);
                    }
            }
        }
	}
}
