﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ants;

namespace FindPattern
{
    public class Rotation90 : TranslationWithZero
    {
        public Rotation90(int size, Loc zero) : base(size, zero) { }

        public override Ants.Loc Translate(Loc origin)
        {
            var x = (origin.Col - zero.Col + size) % size;
            var y = (origin.Row - zero.Row + size) % size;
            return new Loc(origin.Row - y + x, origin.Col - x + (size - 1 - y));
        }

        public override Loc GetSizeRestriction()
        {
            return new Loc(size, size); //size = width = height
        }
    }

    public class Rotation270 : TranslationWithZero
    {
        public Rotation270(int size, Loc zero) : base(size, zero) { }

        public override Ants.Loc Translate(Loc origin)
        {
            var x = (origin.Col - zero.Col + size) % size;
            var y = (origin.Row - zero.Row + size) % size;
            return new Loc(origin.Row - y + (size - 1 - x), origin.Col - x + y);
        }

        public override Loc GetSizeRestriction()
        {
            return new Loc(size, size); //size = width = height
        }
    }

    public class Rotation180 : TranslationWithZero
    {        
        private int height;
        public Rotation180(int width, int height, Loc zero)  : base(width, zero) 
        {            
            this.height = height;
        }

        public override Ants.Loc Translate(Loc origin)
        {
            var width = size;
            var x = (origin.Col - zero.Col + width) % width;
            var y = (origin.Row - zero.Row + height) % height;
            return new Loc(origin.Row - y + (height - 1 - y), origin.Col - x + (width - 1 - x));
        }

        public override Loc GetSizeRestriction()
        {
            return new Loc(height, size);
        }

        public override string ToString()
        {
            return String.Concat(base.ToString(), ";width=", size.ToString(), ";height=", height.ToString());
        }
    }
}
