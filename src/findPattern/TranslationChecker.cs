﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ants;

namespace FindPattern
{
    public interface ITranslationChecker
    {
        bool IsTranslationValid(Translation translation);
    }

    public class TranslationChecker : ITranslationChecker
	{
		Map map;
        Loc[] known;
		int attempts;

		public TranslationChecker(Map map, int attempts)
		{
            var random = new Random();
			this.map = map;
			this.attempts = attempts;
            known = map.Known.Shuffle(random).Take(attempts).ToArray();
		}

		public bool IsTranslationValid(Translation translation)
		{
            int matches = 0;
            
            foreach (Loc knownLoc in known)
            {
                Loc loc = knownLoc;                
                var queue = new Queue<Loc>();
                var visited = new Ants.HashSet<Loc>();                

                for (int i = 0; i < attempts; i++)                
                {
                    Loc other = translation.Translate(loc);

                    if (map.Known.Contains(other))
                    {
                        if (map[loc] == map[other])
                        {
                            if (matches++ > attempts)
                                return true;

                            foreach (var dir in Ants.Ants.Aim.Values)
                            {
                                Loc newLoc = loc.Add(dir);
                                if (!visited.Contains(newLoc) && map.Known.Contains(newLoc))
                                {
                                    queue.Enqueue(newLoc);
                                    visited.Add(newLoc);
                                }
                            }                            
                        }
                        else
                            return false;                        
                    }


                    if (queue.Count > 0)
                        loc = queue.Dequeue();
                    else
                        break;
                }
            }

            return false;
		}
	}
}
