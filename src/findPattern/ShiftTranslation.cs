﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ants;

namespace FindPattern
{
    public class HorizontalShift : TranslationWithSize
	{
        public HorizontalShift(int size) : base(size) { }

        public override Ants.Loc Translate(Loc origin)
		{
            return new Loc(origin.Row, origin.Col + size);
		}

        public override Loc GetSizeRestriction()
        {
            return new Loc(0, size);
        }
	}

    public class VerticalShift : TranslationWithSize
    {
        public VerticalShift(int size) : base(size) { }

        public override Ants.Loc Translate(Loc origin)
        {
            return new Loc(origin.Row + size, origin.Col);
        }

        public override Loc GetSizeRestriction()
        {
            return new Loc(size, 0);
        }
    }
}
