﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ants;
using System.IO;
using System.Drawing;

namespace FindPattern
{
	public class Map
	{
		private Tile[,] tiles;
		private ICollection<Loc> known;

		public ICollection<Loc> Known
		{
			get { return known; }
			set { known = value; }
		}
		private int height;

		public int Height
		{
			get { return height; }
			set { height = value; }
		}
		private int width;

		public int Width
		{
			get { return width; }
			set { width = value; }
		}

		public Tile this[int x, int y]{
			get
			{
				y = y % height;
				if (y < 0) y += height; // because the modulo of a negative number is negative

				x = x % width;
				if (x < 0) x += width;

				return tiles[x, y];
			}
		}

		public Tile this[Loc loc]
		{
			get
			{
				return this[loc.Col, loc.Row];
			}
		}

        public Map Transpose()
        {
            return null;
        }

		public Map(string mapFilename, string maskFilename)
		{
			int currentRow = 0;

			foreach (string line in File.ReadAllLines(mapFilename))
			{
				var pair = line.Split(' ');
				switch (pair[0])
				{ 
					case "rows":
						height = int.Parse(pair[1]);
						break;

					case "cols":
						width = int.Parse(pair[1]);
						break;

					case "m":
						int currentCol = 0;

						if (tiles == null)
						{
							tiles = new Tile[width, height];
							known = new System.Collections.Generic.HashSet<Loc>();
						}

						foreach (char c in pair[1])
						{
							tiles[currentCol++, currentRow] = c == '%' ? Tile.Water : Tile.Land;
							//for now mask is not used, i.e. all tiles are considered as known
							//known.Add(new Loc(currentRow, currentCol));
						}

						currentRow++;
						break;
				}
			}


            Bitmap bitmap = (Bitmap)Image.FromFile(maskFilename);
            int dx = (width - bitmap.Width) / 2;
            int dy = (height - bitmap.Height) / 2;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    if (bitmap.GetPixel(i - dy, j - dx).ToArgb() != Color.Black.ToArgb())
                    {
                        Known.Add(new Loc(j, i));
                    }
                }                 
            }
		}	
	}
}
