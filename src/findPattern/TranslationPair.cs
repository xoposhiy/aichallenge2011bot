﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ants;

namespace FindPattern
{
    public class TranslationPair : Tuple<Translation, Translation>
    {
        public int Rate { get { return size == null ? 0 : size.Row * size.Col; } }
        private Loc size;
        public Loc Size { get { return size; } }

        public TranslationPair(Translation item1, Translation item2, int mapWidth, int mapHeight)
            : base(item1, item2)
        {
            var size1 = item1 == null ? new Loc(0, mapWidth) : item1.GetSizeRestriction();
            var size2 = item2 == null ? new Loc(0, mapHeight) : item2.GetSizeRestriction();

            size = new Loc(Math.Max(size1.Row, size2.Row), Math.Max(size1.Col, size2.Col));
            size = new Loc(size.Row > 0 ? size.Row : mapHeight, size.Col > 0 ? size.Col : mapWidth);            
        }
    }
}
