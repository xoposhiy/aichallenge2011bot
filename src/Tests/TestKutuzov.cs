﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ants;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
	[TestClass]
	public class TestKutuzov
	{
		private List<Direction> expectedOrders;
		private List<Loc> myAnts;
		private GameState state;

		[TestInitialize]
		public void Initialize()
		{
			state = GameState.CreateTest();
			expectedOrders = new List<Direction>();
			myAnts = new List<Loc>();
		}

		[TestMethod]
		public void NoneShouldBeUsed()
		{
			Enemy(1, 0);
			Me(1, 4, Direction.West);
			Me(0, 3, Direction.None);
			Check();
		}

		[TestMethod]
		public void Atack()
		{
			Enemy(1, 0);
			Me(2, 3, Direction.West);
			Me(1, 3, Direction.West);
			Me(0, 3, Direction.West);
			Check();
		}

		[TestMethod]
		public void AtackX()
		{
			state.AddWater(0, 0);
			Enemy(4, 0);
			Enemy(4, 1);
			Me(1, 0, Direction.East);
			Me(2, 3, Direction.West);
			Check();
		}

		[TestMethod]
		public void Defence()
		{
			Enemy(1, 0);
			Me(1, 3, Direction.None);
			Me(0, 3, Direction.None);
			Check();
		}

		[TestMethod]
		public void AttackFromTwoSides()
		{
			Enemy(1, 0);
			Me(2, 3, Direction.West);
			Me(4, 1, Direction.North);
			Check();
		}

		[TestMethod]
		public void Test_2x2()
		{
			Enemy(1, 0);
			Enemy(0, 0);
			Me(1, 3, Direction.None);
			Me(0, 3, Direction.None);
			Check();
		}

		[TestMethod]
		public void Test_2x3()
		{
			Enemy(1, 0);
			Enemy(0, 0);
			Me(1, 3, Direction.None);
			Me(0, 3, Direction.North);
			Me(2, 3, Direction.None);
			Check();
		}

		[TestMethod]
		public void Test_2x3_sandwich()
		{
			Enemy(1, 0);
			Enemy(1, 6);
			Me(0, 3, Direction.East);
			Me(1, 3, Direction.East);
			Me(2, 3, Direction.East);
			Check();
		}


		[TestMethod]
		public void Test_3x3_sandwich()
		{
			Enemy(1, 0);
			Enemy(0, 6);
			Enemy(1, 6);
			Me(1, 3, Direction.West);
			Me(0, 3, Direction.West);
			Me(2, 3, Direction.West);
			Check();
		}

		[TestMethod]
		public void Test_3x4_sandwich()
		{
			Enemy(1, 0);
			Enemy(0, 6);
			Enemy(1, 6);
			Me(1, 3, Direction.West);
			Me(0, 3, Direction.West);
			Me(2, 3, Direction.South);
			Me(3, 3, Direction.West);
			Check();
		}

		[TestMethod]
		public void Test_4x4_sandwich()
		{
			Enemy(1, 0);
			Enemy(0, 6);
			Enemy(1, 6);
			Enemy(5, 3);
			Me(1, 3, Direction.None);
			Me(0, 3, Direction.None);
			Me(2, 3, Direction.South);
			Me(3, 3, Direction.South);
			Check();
		}

		public void Enemy(int row, int col)
		{
			state.AddAnt(row, col, 1);
		}

		public void Me(int row, int col, Direction expectedOrder)
		{
			Ant a = state.AddAnt(row, col, 0);
			myAnts.Add(a);
			expectedOrders.Add(expectedOrder);
		}

		public void Check()
		{
			//TODO
			//			var kutuzov = new Kutuzov(state.EnemyAnts.Cast<Ant>().ToArray(), myAnts.Cast<Ant>().ToArray(), state, 0.3);
//			var positions = kutuzov.MakePlan(50000);
//			Console.WriteLine(positions);
//			foreach (var locOrder in positions.GetAll())
//				Assert.AreEqual(expectedOrders[myAnts.IndexOf(locOrder.Key)], locOrder.Value);
//			Assert.AreEqual(positions.Orders.Length, myAnts.Count);
		}
	}
}