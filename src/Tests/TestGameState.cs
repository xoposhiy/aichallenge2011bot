﻿using System;
using Ants;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
	/*
	[TestClass]
	public class TestGameState
	{
		[TestMethod]
		public void IsInAttackingRangeOnNextTurn()
		{
			var state = GameState.CreateTest();
			Assert.IsFalse(state.IsInAttackRange(new Loc(0, 0), new Loc(0, 3)));
			Assert.IsTrue(state.IsInAttackRange(new Loc(0, 0), new Loc(0, 2)));
			Assert.IsFalse(state.IsInAttackRangeOnNextTurn(new Loc(0, 0), new Loc(0, 5)));
			Assert.IsTrue(state.IsInAttackRangeOnNextTurn(new Loc(0, 0), new Loc(0, 4)));
			Assert.IsTrue(state.IsInAttackRangeOnNextTurn(new Loc(0, 0), new Loc(0, 3)));
			Assert.IsTrue(state.IsInAttackRangeOnNextTurn(new Loc(0, 0), new Loc(0, 2)));
			Assert.IsTrue(state.IsInAttackRangeOnNextTurn(new Loc(0, 0), new Loc(2, 3)));
		}
		
		[TestMethod]
		public void GetDelta()
		{
			var state = GameState.CreateTest(10, 100);
			Action<Loc, Loc, Loc> checkDelta = (expectedDelta, a, b) => Assert.AreEqual(expectedDelta, state.GetDelta(a, b));
			checkDelta(L(0, 0), L(1, 1), L(1, 1));
			checkDelta(L(-2, -1), L(3, 2), L(1, 1));
			checkDelta(L(2, 1), L(1, 1), L(3, 2));
			checkDelta(L(-2, -3), L(1, 1), L(99, 8));
			checkDelta(L(2, 3), L(99, 8), L(1, 1));
		}

		private Loc L(int x, int y)
		{
			return new Loc(x, y);
		}

		[TestMethod]
		public void FindPath_SimpleCases()
		{
			var state = GameState.CreateTest();
			var path = state.FindPath(new Loc(0, 0), new Loc(0, 0));
			Assert.AreEqual(0, path.Count);

			path = state.FindPath(new Loc(0, 0), new Loc(1, 0));
			Assert.AreEqual(1, path.Count);
			Assert.AreEqual(new PathItem(new Loc(0,0), new Loc(1, 0),Direction.South), path[0]);

			int stepsDone;
			path = state.FindPath(new Loc(0, 0), new Loc(4, 4), 7, out stepsDone);
			Assert.IsNull(path);
			Assert.AreEqual(7, stepsDone);
		}

		[TestMethod]
		public void FindPath_AStarOptimality()
		{
			var state = GameState.CreateTest();
			int stepsDone;
			var path = state.FindPath(new Loc(0, 0), new Loc(4, 4), 100, out stepsDone);
			Assert.AreEqual(8, path.Count);
			Assert.AreEqual(8, stepsDone);

			state.AddWater(0, 1);
			state.AddWater(1, 1);
			state.AddWater(2, 1);
			state.AddWater(3, 1);
			path = state.FindPath(new Loc(0, 0), new Loc(4, 4), 100, out stepsDone);
			Assert.AreEqual(8, path.Count);
			Assert.AreEqual(8, stepsDone);

			state.AddWater(4, 1);
			path = state.FindPath(new Loc(0, 0), new Loc(4, 4), 100, out stepsDone);
			Assert.AreEqual(10, path.Count);
			Assert.AreEqual(10, stepsDone);

		}
		[TestMethod]
		public void FindPath_AStarOptimality2()
		{
			// . . ...
			// . # ...
			// A # B..
			// . # ...
			// . . ...
			// .......

			var state = GameState.CreateTest();
			int stepsDone;
			state.AddWater(1, 1);
			state.AddWater(2, 1);
			state.AddWater(3, 1);
			var path = state.FindPath(new Loc(2, 0), new Loc(2, 2), 100, out stepsDone);
			Assert.AreEqual(6, path.Count);
			Assert.IsTrue(stepsDone>=8 && stepsDone<=9);
		}

		[TestMethod]
		public void FindPath_AStarOptimality3()
		{
			// . . . . ..
			// . # . . ..
			// A . # B ..
			// . # . . ..
			// . . . . ..
			// .......

			var state = GameState.CreateTest();
			int stepsDone;
			state.AddWater(1, 1);
			state.AddWater(2, 2);
			state.AddWater(3, 1);
			var path = state.FindPath(new Loc(2, 0), new Loc(2, 3), 100, out stepsDone);
			Assert.AreEqual(7, path.Count);
			Assert.IsTrue(stepsDone >= 10 && stepsDone <= 11);
		}
	}
  */
}
