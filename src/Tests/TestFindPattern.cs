﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FindPattern;
using Ants;

namespace Tests
{
    [TestClass]
    public class TestFindPattern
    {
        static int zero = 0;
        static Loc origin = new Loc(zero, zero);

        [TestMethod]
        public void MinusMirror()
        {
            var t = new MinusMirror(100, zero);
            Assert.AreEqual(new Loc(69, 220), t.Translate(new Loc(30, 220)));
        }

        [TestMethod]
        public void lMirror()
        {
            var t = new lMirror(100, zero);
            Assert.AreEqual(new Loc(30, 279), t.Translate(new Loc(30, 220)));
        }

        [TestMethod]
        public void SlashMirror()
        {
            var t = new SlashMirror(2, origin);
            Assert.AreEqual(new Loc(1, 1), t.Translate(new Loc(0, 0)));
            Assert.AreEqual(new Loc(1, 0), t.Translate(new Loc(1, 0)));
            Assert.AreEqual(new Loc(0, 1), t.Translate(new Loc(0, 1)));
            Assert.AreEqual(new Loc(0, 0), t.Translate(new Loc(1, 1)));

            t = new SlashMirror(3, origin);
            Assert.AreEqual(new Loc(2, 2), t.Translate(new Loc(0, 0)));
            Assert.AreEqual(new Loc(2, 1), t.Translate(new Loc(1, 0)));
            Assert.AreEqual(new Loc(1, 1), t.Translate(new Loc(1, 1)));
            Assert.AreEqual(new Loc(2, 0), t.Translate(new Loc(2, 0)));


            t = new SlashMirror(100, origin);
            Assert.AreEqual(new Loc(79, 269), t.Translate(new Loc(30, 220)));
        }

        [TestMethod]
        public void BackslashMirror()
        {
            var t = new BackslashMirror(100, origin);
            Assert.AreEqual(new Loc(20, 230), t.Translate(new Loc(30, 220)));
        }

        [TestMethod]
        public void Rotation90()
        {
            var t = new Rotation90(100, origin);
            Assert.AreEqual(new Loc(20, 269), t.Translate(new Loc(30, 220)));
        }

        [TestMethod]
        public void Rotation180()
        {
            var t = new Rotation180(100, 50, origin);
            Assert.AreEqual(new Loc(19, 279), t.Translate(new Loc(30, 220)));
        }

        [TestMethod]
        public void Rotation270()
        {
            var t = new Rotation270(100, origin);
            Assert.AreEqual(new Loc(79, 230), t.Translate(new Loc(30, 220)));
        }

        [TestMethod]
        public void CompositeTramslations()
        {
            var l = new Loc(123, 456);
            Assert.AreEqual(new Rotation90(100, origin).Combine(new Rotation90(100, origin)).Translate(l),
                new Rotation180(100, 100, origin).Translate(l));
            Assert.AreEqual(new Rotation90(100, origin).Combine(new Rotation90(100, origin)).Combine(new Rotation90(100, origin)).Translate(l),
                new Rotation270(100, origin).Translate(l));
            Assert.AreEqual(new SlashMirror(100, origin).Combine(new BackslashMirror(100, origin)).Translate(l),
                new Rotation180(100, 100, origin).Translate(l));
            Assert.AreEqual(new MinusMirror(100, zero).Combine(new lMirror(100, zero)).Translate(l),
                new Rotation180(100, 100, origin).Translate(l));
            Assert.AreEqual(new Loc(178, 544),
                new VerticalShift(55).Combine(new HorizontalShift(88)).Translate(l));
        }

        private class TranslationCheckerStub1 : ITranslationChecker
        {
            public bool IsTranslationValid(Translation translation)
            {
                return false;
            }
        }       

        [TestMethod]
        public void FirstLevelTranslations()
        {
            var tf = new TranslationFinder(126, 200, new TranslationCheckerStub1());
            tf.Search(1000000);
            Assert.AreEqual(101461, tf.CheckedCount);
        }

        private class TranslationCheckerStub2 : ITranslationChecker
        {
            public bool IsTranslationValid(Translation translation)
            {
                return true;
            }
        }

        [TestMethod]
        public void SecondLevelTranslations()
        {
            var tf = new TranslationFinder(126, 200, new TranslationCheckerStub2());
            tf.Search(1000000);
            Assert.AreEqual(3002296, tf.CheckedCount);
        }
    }
}
