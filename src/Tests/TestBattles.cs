﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Ants;
using GatherBattles;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class TestBattles
    {
		[TestInitialize]
		public void Setup()
		{
			battles = BattlesReader.ReadBattles(@"c:\work\ants\src\GatherBattles\bin\Debug\battles.txt")
				.ToArray();
			Console.WriteLine(@"Battle situations: {0}", battles.Length);
		}

		[TestMethod]
		public void Calculate()
		{
			Log.Enable = false;
			Calc(1.1, 6);
			Calc(1.1, 5);
//			Calc(0.9, 6);
//			Calc(0.9, 5);
//			Calc(0.2, 6);
//			Calc(0.2, 5);

		}

    	private void Calc(double aggression, int maxDynAnts)
    	{
    		Console.Write(@"Ants: {0}  Aggr: {1}  ", maxDynAnts, aggression);
			var score = new Score(0, 0);
    		score = battles.SelectMany(b => RunBattle(b, maxDynAnts, aggression)).Aggregate(score,
    		                                                                                (current, battleScore) =>
    		                                                                                current.Add(battleScore));
    		Console.WriteLine(score);
    	}

    	private Battle[] battles;

    	private IEnumerable<Score> RunBattle(Battle battle, int maxDynAnts, double aggression)
    	{
			for (int myPlayer = 0; myPlayer < battle.Ants.Length; myPlayer++)
			{
				var state = CreateGameState(battle, myPlayer);
				var used = new Ants.HashSet<Loc>();
				var commander = new BattleCommander(state, new BattleSplitter(state, used, maxDynAnts), new BattlePartSolver(state, null, aggression), () => false);
				commander.CommandAllBattles(used);
				MoveEnemies(battle, myPlayer, state);
				var attackersProviders = battle.Ants.Select(
					(playerAnts, index) => new AttackersProvider(
						index == 0 ? state.MyAnts.Cast<Ant>().ToArray() : state.EnemyAnts.Cast<Ant>().Where(a => a.Team == index+1).ToArray(), 
						state)).ToArray();
				var deads = AttackersProvider.GetDeadsByPlayerIndex(attackersProviders);
				yield return new Score(deads[myPlayer], deads.Where((d, playerIndex) => playerIndex != 0).Sum());
			}
		}

    	private static GameState CreateGameState(Battle battle, int myPlayer)
    	{
			var state = GameState.CreateTest(battle.Cols, battle.Rows, true);
			for (int player = 0; player < battle.Ants.Length; player++)
				foreach (var ant in battle.Ants[player])
					state.AddAnt(ant.Ant.Row, ant.Ant.Col, player == myPlayer ? 0 : player + 1);
    		foreach (var playerAnts in battle.Ants)
				foreach (var ant in playerAnts)
				{
					foreach (var direction in Ants.Ants.Aim.Keys)
					{
						if (!ant.Dirs.Contains(direction))
						{
							var dest = state.GetDestination(ant.Ant, direction);
							if (state[dest] == Tile.Land) state.AddWater(dest.Row, dest.Col);
						}
					}
				}
			return state;
    	}

    	private void MoveEnemies(Battle battle, int myPlayer, GameState state)
    	{
			for (int playerIndex = 0; playerIndex < battle.Ants.Length; playerIndex++)
    		{
				if (playerIndex == myPlayer) continue;
				foreach (var ant in battle.Ants[playerIndex])
    			{
    				state.RemoveAnt(ant.Ant);
    				state.AddAnt(ant.To.Row, ant.To.Col, playerIndex+1);
    			}
    		}
    	}
    }

	public class Score
	{
		public readonly int Deads;
		public readonly int Kills;

		public Score(int deads, int kills)
		{
			Deads = deads;
			Kills = kills;
		}

		public Score Add(Score score)
		{
			return new Score(score.Deads + Deads, score.Kills + Kills);
		}

		public override string ToString()
		{
			return string.Format("Deads: {0}  Kills: {1}  Ratio: {2:#.#}  Aggression: {3}", Deads, Kills, ((double)Kills) / (Deads + 0.000001), Deads + Kills);
		}
	}

	public class BattlesReader
	{
		public static IEnumerable<Battle> ReadBattles(string filename)
		{
			var battleString = "";
			foreach (var line in File.ReadLines(filename))
			{
				if (line.StartsWith("{"))
				{
					if (!string.IsNullOrWhiteSpace(battleString)) 
						yield return new Battle(battleString);
					battleString = line;
				}
				else battleString += line;
			}
			if (!string.IsNullOrWhiteSpace(battleString))
				yield return new Battle(battleString);
		}
	}
}
