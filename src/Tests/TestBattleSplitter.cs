﻿using System;
using System.Linq;
using Ants;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
	[TestClass]
	public class TestBattleSplitter
	{
		private GameState state;
		private Ants.HashSet<Loc> used;
		private BattleSplitter splitter;
		private int battleIndex;
		private BattlePart[] battles;

		[TestInitialize]
		public void Setup()
		{
			state = GameState.CreateTest();
			used = new Ants.HashSet<Loc>();
			splitter = new BattleSplitter(state, used, 4);
			battleIndex = 0;
			battles = null;
		}

		[TestMethod]
		public void NoBattles()
		{
			state.AddAnt(0, 0, 0);
			state.AddAnt(1, 0, 0);
			state.AddAnt(5, 5, 1);
			state.AddAnt(6, 5, 1);
			Assert.IsFalse(splitter.SplitIntoBattleParts().Any());
		}

		[TestMethod]
		public void OneSimpleBattle()
		{
			var my = state.AddAnt(0, 0, 0);
			var his = state.AddAnt(4, 0, 1);
			CheckOneBattle(a(my), a(his));
		}

		[TestMethod]
		public void OneWithStatic()
		{
			var my1 = state.AddAnt(0, 0, 0);
			var my2 = state.AddAnt(0, 1, 0);
			var my3 = state.AddAnt(1, 2, 0);
			var his1 = state.AddAnt(4, 0, 1);
			var his2 = state.AddAnt(4, 1, 1);
			var his3 = state.AddAnt(4, 2, 1);
			var his4 = state.AddAnt(4, 3, 1);
			CheckOneBattle(a(my1, my2), a(his1, his2), a(my3), a(his3));
		}

		[TestMethod]
		public void TwoBattles()
		{
			var my1 = state.AddAnt(0, 0, 0);
			var his1 = state.AddAnt(4, 0, 1);
			var my2 = state.AddAnt(0, 5, 0);
			var his2 = state.AddAnt(4, 5, 1);
			CheckOneBattle(a(my1), a(his1));
			CheckOneBattle(a(my2), a(his2));
		}

		[TestMethod]
		public void TwoBattlesWithCommonStaticAnts()
		{
			/* A . . . E
			 * A . . . E
			 * A . . . E
			 * A . . . E
			 */
			var my1 = state.AddAnt(0, 0, 0);
			var my2 = state.AddAnt(1, 0, 0);
			var my3 = state.AddAnt(2, 0, 0);
			var my4 = state.AddAnt(3, 0, 0);
			var his1 = state.AddAnt(0, 4, 1);
			var his2 = state.AddAnt(1, 4, 1);
			var his3 = state.AddAnt(2, 4, 1);
			var his4 = state.AddAnt(3, 4, 1);
			CheckOneBattle(a(my1, my2), a(his1, his2), a(my3), a(his3));
			CheckOneBattle(a(my3), a(his2, his3, his4), a(my1, my2, my4), a());
		} 

		private void CheckOneBattle(Ant[] expectedMyDynamic, Ant[] expectedEnemyDynamic, Ant[] expectedMyStatic = null, Ant[] expectedEnemyStatic = null)
		{
			if (expectedEnemyStatic == null) expectedEnemyStatic = new Ant[0];
			if (expectedMyStatic == null) expectedMyStatic = new Ant[0];
			battles = battles ?? splitter.SplitIntoBattleParts().ToArray();
			var battle = battles[battleIndex++];
			Console.WriteLine("check");
			foreach (var ant in battle.DynamicAnts)
				Console.WriteLine(ant);
			Console.WriteLine();
			foreach (var ant in battle.StaticAnts)
				Console.WriteLine(ant);
			CollectionAssert.AreEquivalent(expectedMyDynamic, battle.GetMyDynamic().ToList(), "my dynamic");
			CollectionAssert.AreEquivalent(expectedEnemyDynamic, battle.DynamicAnts.Where(a1 => a1.Team != 0).ToList(), "enemy dynamic");
			CollectionAssert.AreEquivalent(expectedMyStatic, battle.GetMyStatic().ToList(), "my dynamic");
			CollectionAssert.AreEquivalent(expectedEnemyStatic, battle.StaticAnts.Where(a1 => a1.Team != 0).ToList(), "enemy static");
		}

		private T[] a<T>(params T[] items)
		{
			return items;
		}

		private Ant[] a()
		{
			return new Ant[0];
		}
	}
}
