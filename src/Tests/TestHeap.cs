﻿using System;
using System.Diagnostics;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Ants;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
	[TestClass]
	public class TestHeap
	{
		[TestMethod]
		public void AddExtract()
		{
			var heap = new Heap<int>(i => i);
			heap.Add(1);
			heap.Add(2);
			heap.Add(0);
			heap.Add(3);
			Assert.AreEqual(3, heap.ExtractBest());
			Assert.AreEqual(2, heap.ExtractBest());
			Assert.AreEqual(1, heap.ExtractBest());
			Assert.AreEqual(0, heap.ExtractBest());
		}

		[TestMethod]
		public void Performance()
		{
			var count = 10000;
			var sw = Stopwatch.StartNew();
			var heap = new Heap<int>(i => i);
			for (int i = 0; i < count; i += 2)
				heap.Add(i);
			for (int i = count-1; i >= 0; i -= 2)
				heap.Add(i);
			for (int i = count-1; i >= 0; i--)
				Assert.AreEqual(i, heap.ExtractBest());
			var elapsed = sw.ElapsedMilliseconds;
			Console.WriteLine("{0} ms / op", (double)elapsed / (2*count));
		}
	}
}
