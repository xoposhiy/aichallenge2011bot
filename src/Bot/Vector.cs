using System;
using System.Collections.Generic;

namespace Ants
{
	public class Vector
	{
		public static Vector Zero = new Vector(0, 0);
		public readonly double Fx, Fy;

		public Vector(double fx, double fy)
		{
			Fx = fx;
			Fy = fy;
		}

		public static Vector ForceR2(Loc subject, Loc attractor, double value, IGameState state)
		{
			Loc d = state.GetDelta(subject, attractor);
			if (d.Row == 0 && d.Col == 0) return new Vector(0, 0);

			double len = d.ManhatLen();
			double len3 = len*len*len;
			return new Vector(value*d.Col/len3, value*d.Row/len3);
		}

		public Vector Invert()
		{
			return new Vector(-Fx, -Fy);
		}

		public Vector Add(Vector force)
		{
			return new Vector(Fx + force.Fx, Fy + force.Fy);
		}

		public Direction[] ToDirections()
		{
			if (Math.Abs(Fx) < 1e-6 && Math.Abs(Fy) < 1e-6) return new Direction[0];
			Direction dx = Fx > 0 ? Direction.East : Direction.West;
			Direction dy = Fy > 0 ? Direction.South : Direction.North;
			return Math.Abs(Fx) >= Math.Abs(Fy) ? new[] {dx, dy} : new[] {dy, dx};
		}

		public override string ToString()
		{
			return string.Format("Fx: {0}, Fy: {1}", Fx, Fy);
		}

		public double GetModule()
		{
			return Math.Sqrt(Fx*Fx + Fy*Fy);
		}
	}

	public static class ForceExtensions
	{
		public static Vector AttractionR2Force(this IEnumerable<Loc> attractors, Loc subject, double value, IGameState state)
		{
			Vector f = Vector.Zero;
			foreach (Loc attractor in attractors)
			{
				f = f.Add(Vector.ForceR2(subject, attractor, value, state));
			}
			return f;
		}
	}

}