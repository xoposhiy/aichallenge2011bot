using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Ants
{
	public static class Log
	{
		private static readonly TextWriter logFile;
#if DEBUG
		public static bool Enable = false;
#else
		public static bool Enable = false;
#endif

		static Log()
		{
			if (!Enable) return;
			var name = Process.GetCurrentProcess().ProcessName;
			var id = Process.GetCurrentProcess().Id;
			logFile = new StreamWriter("log-" + name + "." + id + ".txt", false);
		}

		public static void FlushLog(this IGameState state)
		{
			if (!Enable) return;
			var sw = Stopwatch.StartNew();
			logFile.Flush();
			state.L("Log.Flush finished in {0} ms", sw.ElapsedMilliseconds);
		}

		public static void L(this IGameState state, string s)
		{
			//Console.WriteLine(state.TurnNo + "\t " + s);
			if (!Enable) return;
			logFile.WriteLine(state.TurnNo + "\t " + s);
		}

		public static void L(this IGameState state, string format, params object[] args)
		{
			if (!Enable) return;
			logFile.WriteLine(state.TurnNo + "\t " + string.Format(format, args));
		}

		public static void LogFullState(this IGameState state)
		{
			if (!Enable) return;
			state.L("MyHills {0}", state.MyHills.Count);
			state.L("EnemyHills {0}", state.EnemyHills.Count);
			state.L("MyAnts {0}", state.MyAnts.Count);
			state.L("EnemyAnts {0}", state.EnemyAnts.Count);
			state.L("Food {0}", state.FoodTiles.Count);
		}

		private static void LogLocs(this IGameState state, string name, ICollection<Loc> locs)
		{
			state.L("{0} ({1}): {2}", name, locs.Count, locs.Aggregate(string.Empty, (s, loc) => s + loc + " "));
		}
	}
}