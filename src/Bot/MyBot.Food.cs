using System;

namespace Ants
{
	public partial class MyBot
	{
		private void DoGatherFood(HashSet<Loc> antsBusyInThisTurn, HashSet<Loc> engagedFood)
		{
			var food = new HashSet<Loc>(State.FoodTiles);
			var freeAnts = GetAntsNotBusyInThisTurn(antsBusyInThisTurn);
			var paths = State.FindNonOverlappingPaths(food, freeAnts, true, () => food.Count == 0 || TimeToFinishTurn, State.GetLandDirections);
			foreach (var path in paths)
			{
				engagedFood.Add(path.Target);
				GatherFood(path, antsBusyInThisTurn);
			}
		}

		private void GatherFood(Path path, HashSet<Loc> antsBusyInThisTurn)
		{
			var ant = path.Ant;
			var food = path.Target;
			var dir = path.DirectionOfTheFirstMove;
			if (MoveIsDangerous(ant, dir)) return;
			State.L(string.Format("GatherFood: ant {0}, dir {1}, food {2}", ant, dir, food));
			State.IssueOrder(ant, dir, antsBusyInThisTurn);
			V.MarkAnt(path.PathItems[0].To, 100, 60, 60);
			V.MarkFoodPath(path.PathItems);
		}
	}
}