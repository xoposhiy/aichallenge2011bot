using System;
using System.Collections.Generic;

namespace Ants
{
	public interface IBattlePlan
	{
		int CalculatedPositions { get; }
		int EnemyDeads { get; }
		int MyDeads { get; }
		double WinAngle { get; }
		IDictionary<Loc, Direction> Directions { get; }
	}

	public class BattlePlan : IBattlePlan
	{
		public BattlePlan(Dictionary<Loc, Direction> orders)
		{
			Directions = new Dictionary<Loc, Direction>();
			foreach (var pair in orders)
				Directions.Add(pair.Key, pair.Value);
		}

		public IDictionary<Loc, Direction> Directions { get; private set;}
		public int CalculatedPositions { get;  set; }
		public int EnemyDeads { get;  set; }
		public int MyDeads { get;  set; }

		public override string ToString()
		{
			return string.Format("WinAngle: {0}, CalculatedPositions: {1}, EnemyDeads: {2}, MyDeads: {3}", WinAngle, CalculatedPositions, EnemyDeads, MyDeads);
		}

		public double WinAngle { get; set; }

		public void SetWinAngle(int myDeads, int enemyDeads)
		{
			if (myDeads == 0 && enemyDeads == 0) WinAngle = 76; //hmm...
			else if (myDeads == 0) WinAngle = 90;
			else if (enemyDeads == 0) WinAngle = 0;
			else WinAngle = Math.Atan2(enemyDeads, myDeads) * 180 / Math.PI;
		}
	}
}