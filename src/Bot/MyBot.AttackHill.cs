using System.Collections.Generic;
using System.Linq;

namespace Ants
{
	public partial class MyBot
	{
		private Loc hillToAttack;
		private int startAttackTime;

		private void DoAttackEnemyHill(HashSet<Loc> antsBusyInThisTurn)
		{
			var enemyHill = SelectBestEnemyHillToAttack(State.EnemyHills);
			if (enemyHill != null)
			{
				V.MarkAttackingEnemyHill(enemyHill);
				var freeAnts = GetAntsNotBusyInThisTurn(antsBusyInThisTurn);
				var attackersCount = freeAnts.Count / 4;
				var attackers = FindClosestAnts(freeAnts, enemyHill, attackersCount).ToArray();
				State.L("Attacking hill {0} with {1} ants", enemyHill, attackers.Length);
				AttackHill(attackers, enemyHill, antsBusyInThisTurn);
			}
		}

		private void AttackHill(Loc[] attackers, Loc enemyHill, HashSet<Loc> antsBusyInThisTurn)
		{
			var ants = new HashSet<Loc>();
			foreach (var attacker in attackers) ants.Add(attacker);
			var paths = State.FindNonOverlappingPaths(new HashSet<Loc> { enemyHill }, ants, false, () => TimeToFinishTurn, State.GetLandDirections);
			State.L("Attacking paths count: " + paths.Count);
			foreach (var path in paths)
			{
				State.IssueOrder(path.Ant, path.PathItems[0].Direction, antsBusyInThisTurn);
				V.MarkAnt(path.PathItems[0].To, 255, 100, 100);
				V.MarkAttackingPath(path.PathItems);
			}
		}

		/*
				private IEnumerable<Direction> GetDirectionsUnoccupiedByMyAnts(Loc loc)
				{
					return State.GetLandDirections(loc).Where(
						d =>
							{
								var dest = State.GetDestination(loc, d);
								return State.IsLand(dest) && !State.MyAnts.Contains(dest);
							});
				}
		 */

		private Loc SelectBestEnemyHillToAttack(IEnumerable<Loc> enemyHills)
		{
			if (!State.EnemyHills.Contains(hillToAttack) || State.TurnNo - startAttackTime > 10) hillToAttack = null;
			if (hillToAttack != null) return hillToAttack;

			var teamSizes = State.EnemyAnts.Cast<Ant>().Aggregate(
				new Dictionary<int, int>(),
				(counts, loc) =>
				{
					var team = loc.Team;
					counts[team] = counts.SafeGet(team) + 1;
					return counts;
				}
				);
			var minHill = enemyHills.Cast<AntHill>().MinItem(hill => teamSizes.SafeGet(hill.Team));
			startAttackTime = State.TurnNo;
			return hillToAttack = minHill;
		}

	}
}