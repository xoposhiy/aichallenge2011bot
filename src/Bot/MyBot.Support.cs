using System.Linq;

namespace Ants
{
	partial class MyBot
	{
		private EnemyAntDiffusionMap enemyDiffusionMap;

		private void DoSupportAttackers(HashSet<Loc> antsBusyInThisTurn)
		{
			if (TimeToFinishTurn) return;
			if (enemyDiffusionMap == null) enemyDiffusionMap = new EnemyAntDiffusionMap(State);
			enemyDiffusionMap.Update(State);
			if (!TimeToFinishTurn) enemyDiffusionMap.Update(State);
			if (!TimeToFinishTurn) enemyDiffusionMap.Update(State);
			foreach (var ant in GetAntsNotBusyInThisTurn(antsBusyInThisTurn))
			{
				ControlByDiffusionMap(ant, antsBusyInThisTurn);
			}
		}

		private void ControlByDiffusionMap(Loc ant, HashSet<Loc> antsBusyInThisTurn)
		{
			var dirs = State.GetLandDirections(ant).ToArray();
			var direction = dirs.MaxItem(d => enemyDiffusionMap[State.GetDestination(ant, d)]);
			if (enemyDiffusionMap[ant] > 0.000001)
			{
				State.IssueOrder(ant, direction, antsBusyInThisTurn);
				V.MarkAnt(ant, 100, 255, 0);
			}
		}
	}
}