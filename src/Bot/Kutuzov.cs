//using System;
//using System.Collections.Generic;
//using System.Linq;
//
//namespace Ants
//{
//	public class Kutuzov
//	{
//		private readonly IBattleAnt[] enemies;
//		private readonly IBattleAnt[] my;
//		private readonly IGameState state;
//		private readonly double aggression;
//		private readonly double noKillWinAngle;
//
//		/// <summary>
//		/// 
//		/// </summary>
//		/// <param name="enemies"></param>
//		/// <param name="my"></param>
//		/// <param name="state"></param>
//		/// <param name="aggression">����� (myDeads:0, myKills:0) ������������ ������ (myDeads:agression, myKills:1)</param>
//		public Kutuzov(Ant[] enemies, Ant[] my, IGameState state, double aggression)
//		{
//			this.state = state;
//			this.aggression = aggression;
//			noKillWinAngle = Math.Atan2(1, aggression) * 180 / Math.PI;
//			this.enemies = enemies.Select((Ant a) => CreateEnemyUnit(a)).ToArray();
//			this.my = my.Select(me => CreateMyUnit(me, my)).ToArray();
////			state.L("nokillwinangle "+ noKillWinAngle);
//		}
//
//		private IBattleAnt CreateMyUnit(Ant me, IEnumerable<Loc> allMy)
//		{
//			return new Unit(
//				me,
//				new[] { Direction.None }.Concat(
//					state.GetLandDirections(me).Where(
//						d =>
//						{
//							Loc dest = state.GetDestination(me, d);
//							return state[dest] != Tile.Ant || allMy.Contains(dest);
//						})
//					).ToArray()
//					);
//		}
//
//		private IBattleAnt CreateEnemyUnit(Ant enemy)
//		{
//			return new Unit(enemy, new[] { Direction.None }.Concat(state.GetLandDirections(enemy)).ToArray());
//		}
//
//		public PlayerPositions MakePlan(int iterations)
//		{
//			PlayerPositions[] myFinals = GenerateCombinations(my);
//			PlayerPositions[] hisFinals = GenerateCombinations(enemies);
////			state.L("myFinals {0} hisFinals {1}", myFinals.Length, hisFinals.Length);
//			if (myFinals.Length * hisFinals.Length < iterations)
//				return SearchAllCombinations(hisFinals, myFinals);
//			else
//				return RandomizedSearch(iterations, hisFinals, myFinals);
//		}
//
//		private PlayerPositions RandomizedSearch(int iterationsCount, PlayerPositions[] hisFinals, PlayerPositions[] myFinals)
//		{
//			PlayerPositions bestPositions = null;
//			double bestAvgWinAngle = -1;
//			int i = 0;
//			while (i < iterationsCount)
//			{
//				var myPos = myFinals[state.R.Next(myFinals.Length)];
//				double avgWinAngle = CalculateAvgWinAngle(myPos, hisFinals);
//				if (bestAvgWinAngle < avgWinAngle)
//				{
//					bestPositions = myPos;
//					bestAvgWinAngle = avgWinAngle;
//				}
//				i += hisFinals.Length;
//			}
//			return bestPositions;
//		}
//
//		private PlayerPositions SearchAllCombinations(PlayerPositions[] hisFinals, PlayerPositions[] myFinals)
//		{
//			PlayerPositions bestPositions = null;
//			double bestAvgWinAngle = -1;
//			foreach (PlayerPositions myPos in myFinals)
//			{
//				double avgWinAngle = CalculateAvgWinAngle(myPos, hisFinals);
////				state.L("	pos {0} -> {1}", myPos, avgWinAngle);
//				if (bestAvgWinAngle < avgWinAngle)
//				{
//					bestPositions = myPos;
//					bestAvgWinAngle = avgWinAngle;
//				}
//			}
////			state.L("WinAngle = " + bestAvgWinAngle);
//			return bestPositions;
//		}
//
//		private PlayerPositions[] GenerateCombinations(IBattleAnt[] units)
//		{
//			var res = new List<PlayerPositions>();
//			var orders = new Direction[units.Length];
//			var busyLocations = new List<Loc>();
//			SearchCombination(units, 0, orders, busyLocations,
//							  () =>
//							  {
//								  var playerPositions = new PlayerPositions(units.Select(u => u.Ant).ToArray(), orders, state);
//								  res.Add(playerPositions);
//							  });
//			return res.ToArray();
//
//		}
//
//		private double CalculateAvgWinAngle(PlayerPositions myPos, PlayerPositions[] hisPositions)
//		{
//			var avg = 0.0;
//			var min = double.MaxValue;
//			foreach (var t in hisPositions)
//			{
//				var winAngle = CalculateWinAngle(myPos, t);
//				avg += winAngle;
//				if (winAngle < min) min = winAngle;
//			}
//			avg /= hisPositions.Length;
////			Console.WriteLine("my {0} -> min {1} av {2}", myPos, min, avg);
//			return min + (avg / 100);
//		}
//
//		private double CalculateWinAngle(PlayerPositions myPos, PlayerPositions hisPos)
//		{
//			int myDeads = myPos.FinalLocs.Count(ant => IsDead(ant, myPos, hisPos));
//			int hisDeads = hisPos.FinalLocs.Count(ant => IsDead(ant, hisPos, myPos));
//			var winAngle = GetWinAngle(myDeads, hisDeads);
////			Console.WriteLine("my {0} his {1} -> {2} {3} {4}", myPos, hisPos, myDeads, hisDeads, winAngle);
//			return winAngle;
//		}
//
//		private bool IsDead(Ant ant, PlayerPositions myPositions, PlayerPositions enemyPositions)
//		{
//			var attackers = enemyPositions.GetAttackers(ant);
//			return attackers.Any(a => myPositions.GetAttackers(a).Length <= attackers.Length);
//		}
//
//		private double GetWinAngle(int myDeads, int enemyDeads)
//		{
//			// winAngle = arctan(enemyDeads, myDeads)
//			if (myDeads == 0 && enemyDeads == 0) return noKillWinAngle;
//			if (myDeads == 0) return 90;
//			if (enemyDeads == 0) return 0;
//			return Math.Atan2(enemyDeads, myDeads) * 180 / Math.PI;
//		}
//
//		private void SearchCombination(IBattleAnt[] mates, int mateToMakeOrder, Direction[] orders, List<Loc> busyLocations, Action onOrdersReady)
//		{
//			var mate = mates[mateToMakeOrder];
//			foreach (Direction dir in mate.Dirs)
//			{
//				var dest = state.GetDestination(mate.Ant, dir);
//				if (busyLocations.Contains(dest)) continue;
//				orders[mateToMakeOrder] = dir;
//				busyLocations.Add(dest);
//				try
//				{
//					if (mateToMakeOrder == mates.Count() - 1)
//						onOrdersReady();
//					else
//						SearchCombination(mates, mateToMakeOrder + 1, orders, busyLocations, onOrdersReady);
//				}
//				finally
//				{
//					busyLocations.Remove(dest);
//				}
//			}
//		}
//	}
//
//}
//
