using System;

namespace Ants
{
	internal class StageTimeStats
	{
		public StageTimeStats(long elapsed)
		{
			max = avg = sum = elapsed;
			count = 1;
		}

		public StageTimeStats(StageTimeStats old, long elapsed)
		{
			max = Math.Max(old.max, elapsed);
			sum = old.sum + elapsed;
			count = old.count + 1;
			avg = sum/count;
		}

		public override string ToString()
		{
			return String.Format("Max: {0}, Avg: {1}", max, avg);
		}

		public readonly long max;
		public readonly long sum;
		public readonly long count;
		public readonly long avg;
	}
}