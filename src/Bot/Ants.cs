using System;
using System.Globalization;
using System.IO;
using System.Collections.Generic;
using System.Threading;

namespace Ants
{

	public class Ants
	{
		public Ants()
		{
			Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
		}

		public static readonly Loc North = new Loc(-1, 0);
		public static readonly Loc South = new Loc(1, 0);
		public static readonly Loc West = new Loc(0, -1);
		public static readonly Loc East = new Loc(0, 1);

		public static IDictionary<Direction, Loc> Aim = new Dictionary<Direction, Loc> {
			{ Direction.North, North},
			{ Direction.East, East},
			{ Direction.South, South},
			{ Direction.West, West}
		};


		private const string READY = "ready";
		private const string GO = "go";
		private const string END = "end";

		private GameState state;
		public static Random R = new Random();


		public void PlayGame(Bot bot)
		{

			List<string> input = new List<string>();
			try
			{
				while (true)
				{
					string line = System.Console.In.ReadLine().Trim().ToLower();

					if (line.Equals(READY))
					{
						ParseSetup(input);
						bot.State = state;
						FinishTurn();
						input.Clear();
					}
					else if (line.Equals(GO))
					{
						StartTurn();
						ParseUpdate(input);
						state.LogFullState();
						bot.DoTurn();
						input.Clear();
						FinishTurn();
					}
					else if (line.Equals(END))
					{
						break;
					}
					else
					{
						input.Add(line);
					}
				}
			}
			catch (Exception e)
			{
#if DEBUG
				var fs = new FileStream("debug.log", FileMode.Create, FileAccess.Write);
				var sw = new StreamWriter(fs);
				sw.WriteLine(e);
				sw.Close();
				fs.Close();
				state.L("ERROR " + e);
				state.FlushLog();
#endif
			}
		}

		private void StartTurn()
		{
			state.L("================ NextTurn ================");
			state.StartNewTurn();
		}

		// parse initial input and setup starting game state
		private void ParseSetup(List<string> input)
		{
			int width = 0, height = 0;
			int turntime = 0, loadtime = 0;
			int viewradius2 = 0, attackradius2 = 0, spawnradius2 = 0;
			int seed = 0;

			foreach (string line in input)
			{
				if (line.Length <= 0) continue;

				string[] tokens = line.Split();
				string key = tokens[0];

				if (key.Equals(@"cols"))
				{
					width = int.Parse(tokens[1]);
				}
				else if (key.Equals(@"rows"))
				{
					height = int.Parse(tokens[1]);
				}
				else if (key.Equals(@"seed"))
				{
					seed = int.Parse(tokens[1]);
				}
				else if (key.Equals(@"turntime"))
				{
					turntime = int.Parse(tokens[1]);
				}
				else if (key.Equals(@"loadtime"))
				{
					loadtime = int.Parse(tokens[1]);
				}
				else if (key.Equals(@"viewradius2"))
				{
					viewradius2 = int.Parse(tokens[1]);
				}
				else if (key.Equals(@"attackradius2"))
				{
					attackradius2 = int.Parse(tokens[1]);
				}
				else if (key.Equals(@"spawnradius2"))
				{
					spawnradius2 = int.Parse(tokens[1]);
				}
			}

			this.state = new GameState(width, height,
									   turntime, loadtime,
									   viewradius2, attackradius2, spawnradius2, seed);
		}

		// parse engine input and update the game state
		private void ParseUpdate(IEnumerable<string> input)
		{
			// do some stuff first

			foreach (string line in input)
			{
				if (line.Length <= 0) continue;

				string[] tokens = line.Split();

				if (tokens.Length >= 3)
				{
					int row = int.Parse(tokens[1]);
					int col = int.Parse(tokens[2]);

					if (tokens[0].Equals("a"))
					{
						state.AddAnt(row, col, int.Parse(tokens[3]));
					}
					else if (tokens[0].Equals("f"))
					{
						state.AddFood(row, col);
					}
					else if (tokens[0].Equals("r"))
					{
						state.RemoveFood(row, col);
					}
					else if (tokens[0].Equals("w"))
					{
						state.AddWater(row, col);
					}
					else if (tokens[0].Equals("d"))
					{
						state.DeadAnt(row, col);
					}
					else if (tokens[0].Equals("h"))
					{
						state.AntHill(row, col, int.Parse(tokens[3]));
					}
				}
			}
			state.RemoveObsoleteData();
			state.UpdateSeenArea();
		}

		private void FinishTurn()
		{
			state.FlushLog();
			state.L("Unused time " + 100 * state.TimeRemaining / state.TurnTime + "% " + state.TimeRemaining + " of " + state.TurnTime + " ms");
			state.StopTimer();
			Console.Out.WriteLine(GO);
		}
	}
}