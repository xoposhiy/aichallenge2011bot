using System;
using System.Collections.Generic;

namespace Ants {

	public class Loc : IEquatable<Loc>
	{

		/// <summary>
		/// Gets the row of this location.
		/// </summary>
		public readonly int Row;

		/// <summary>
		/// Gets the column of this location.
		/// </summary>
		public readonly int Col;

		public Loc (int row, int col) {
			this.Row = row;
			this.Col = col;
		}

		public int ManhatLen()
		{
			return Math.Abs(Row) + Math.Abs(Col);
		}

		public int Eq2Len()
		{
			return Row * Row + Col * Col;
		}

		public Loc(Loc loc) : this(loc.Row, loc.Col)
		{

		}

        public Loc Add(Loc delta)
        {
            return new Loc(this.Row + delta.Row, this.Col + delta.Col);
        }

		public override bool Equals (object obj) {
			if (ReferenceEquals (null, obj))
				return false;
			if (ReferenceEquals (this, obj))
				return true;

			return ((Loc)obj).Row == this.Row && ((Loc)obj).Col == this.Col;
		}

		public bool Equals (Loc other) {
			if (ReferenceEquals (null, other))
				return false;
			if (ReferenceEquals (this, other))
				return true;

			return other.Row == this.Row && other.Col == this.Col;
		}

		public override int GetHashCode()
		{
			unchecked {
				return (this.Row * 397) ^ this.Col;
			}
		}

		public override string ToString()
		{
			return string.Format("({0}, {1})", Row, Col);
		}
	}

	public class TeamLoc : Loc, IEquatable<TeamLoc> {
		/// <summary>
		/// Gets the team of this ant.
		/// </summary>
		public int Team { get; private set; }

		public TeamLoc(int row, int col, int team)
			: base(row, col)
		{
			this.Team = team;
		}
		public TeamLoc(Loc loc, int team)
			: this(loc.Row, loc.Col, team)
		{
		}

		public bool Equals(TeamLoc other) {
			return base.Equals (other);
		}

	}
	
	public class Ant : TeamLoc, IEquatable<Ant> {
		public Ant (int row, int col, int team) : base (row, col, team) {
		}

		public Ant(Loc loc, int team)
			: this(loc.Row, loc.Col, team)
		{
		}

		public bool Equals (Ant other) {
			return base.Equals (other);
		}

		public Ant Move(Direction move)
		{
			return new Ant(Row + Ants.Aim[move].Row, Col + Ants.Aim[move].Col, Team);
		}
	}

	public class AntHill : TeamLoc, IEquatable<AntHill> {
		public AntHill (int row, int col, int team) : base (row, col, team) {
		}

		public bool Equals (AntHill other) {
			return base.Equals (other);
		}
	}
}

