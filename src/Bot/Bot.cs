
namespace Ants {
	public abstract class Bot {

		public abstract void DoTurn();

		public IGameState State;
	}
}