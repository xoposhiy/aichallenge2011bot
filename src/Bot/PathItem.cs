using System.Collections.Generic;

namespace Ants
{
	public class PathItem
	{
		public PathItem(Loc from, Loc to, Direction direction)
		{
			From = from;
			To = to;
			Direction = direction;
		}

		public Loc From { get; private set; }
		public Loc To { get; private set; }
		public Direction Direction { get; private set; }

		public bool Equals(PathItem other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return Equals(other.From, From) && Equals(other.To, To) && Equals(other.Direction, Direction);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != typeof (PathItem)) return false;
			return Equals((PathItem) obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				int result = (From != null ? From.GetHashCode() : 0);
				result = (result*397) ^ (To != null ? To.GetHashCode() : 0);
				result = (result*397) ^ Direction.GetHashCode();
				return result;
			}
		}

		public override string ToString()
		{
			return string.Format("From: {0}, To: {1}, Direction: {2}", From, To, Direction);
		}
	}

	public class PathFinderQueueItem
	{
		public PathFinderQueueItem(Loc loc, Loc target)
		{
			Loc = loc;
			Target = target;
		}

		public Loc Loc { get; private set; }
		public Loc Target { get; private set; }
	}

	public class Path
	{
		public Path(Loc ant, Loc target, List<PathItem> pathItems)
		{
			Ant = ant;
			Target = target;
			PathItems = pathItems;
		}

		public Loc Ant { get; private set; }
		public Loc Target { get; private set; }
		public List<PathItem> PathItems { get; private set; }

		public Direction DirectionOfTheFirstMove
		{
			get { return PathItems.Count > 1 ? PathItems[0].Direction : Direction.None; }
		}

		public override string ToString()
		{
			return string.Format("{0} -> {1} [{2}]", Ant, Target, PathItems.Count);
		}
	}
}