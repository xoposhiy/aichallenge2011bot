using System.Linq;

namespace Ants
{
	partial class MyBot : Bot
	{
		private void DoGoToBorder(HashSet<Loc> antsBusyInThisTurn)
		{
			var unseen = State.SeenBorder.Shuffle(State.R).ToHashSet();
			HashSet<Loc> ants = GetAntsNotBusyInThisTurn(antsBusyInThisTurn);
			var count = ants.Count / 2;
			var freeAnts = ants.Take(count).ToHashSet();
			var paths = State.FindNonOverlappingPaths(unseen, freeAnts, false, () => unseen.Count == 0 || TimeToFinishTurn, State.GetLandDirections);
			foreach (var path in paths)
				Seen(path, antsBusyInThisTurn);
		}

		private void Seen(Path path, HashSet<Loc> antsBusyInThisTurn)
		{
			var ant = path.Ant;
			var target = path.Target;
			var dir = path.DirectionOfTheFirstMove;
//			State.L(string.Format("Seen: ant {0}, dir {1}, target {2}", ant, dir, target));
			State.IssueOrder(ant, dir, antsBusyInThisTurn);
			V.MarkAnt(path.PathItems[0].To, 50, 100, 200);
			//V.MarkSeenPath(path.PathItems);
		}

	}

}