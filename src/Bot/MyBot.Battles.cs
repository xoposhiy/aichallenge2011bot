namespace Ants
{
	public interface IBattleAnt
	{
		Ant Ant { get; }
		Direction[] Dirs { get; }
	}

	public partial class MyBot
	{
		private void DoBattles(HashSet<Loc> antsBusyInThisTurn)
		{
			var commander = new BattleCommander(State, new BattleSplitter(State, antsBusyInThisTurn, 6), new BattlePartSolver(State, hillToAttack), () => TimeToFinishTurn);
			commander.CommandAllBattles(antsBusyInThisTurn);
		}
	}
}