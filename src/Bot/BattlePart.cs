using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Ants
{
	public class BattlePart
	{
		private readonly int maxDynamicAntsCount;
		private readonly IGameState state;
		private readonly HashSet<Loc> antsUsedThisTurn;
		public readonly HashSet<Ant> DynamicAnts = new HashSet<Ant>();
		public readonly HashSet<Ant> StaticAnts = new HashSet<Ant>();

		public BattlePart(int maxDynamicAntsCount, IGameState state, HashSet<Loc> antsUsedThisTurn)
		{
			this.maxDynamicAntsCount = maxDynamicAntsCount;
			this.state = state;
			this.antsUsedThisTurn = antsUsedThisTurn;
		}

		public void AddAnt(Ant ant)
		{
			if (DynamicAnts.Count < maxDynamicAntsCount && !antsUsedThisTurn.Contains(ant))
				DynamicAnts.Add(ant);
			else
				StaticAnts.Add(ant);
		}

		public bool IsInStatic(Ant ant)
		{
			return StaticAnts.Contains(ant);
		}

		public bool Contains(Ant ant)
		{
			return StaticAnts.Contains(ant) || DynamicAnts.Contains(ant);
		}

		public IEnumerable<Ant> AllEnemyAnts()
		{
			return StaticAnts.Concat(DynamicAnts).Where(ant => ant.Team != 0);
		}

		public IEnumerable<Ant> GetMyDynamic()
		{
			return DynamicAnts.Where(a1 => a1.Team == 0);
		}

		public IEnumerable<Ant> GetMyStatic()
		{
			return StaticAnts.Where(a => a.Team == 0);
		}
	}
}