using System;
using System.Collections.Generic;
using System.Text;

namespace Ants
{
	public class BattleSolution
	{
		public IDictionary<Loc, Direction> Orders = new Dictionary<Loc, Direction>();
		public IDictionary<Loc, double> Aggression = new Dictionary<Loc, double>();
		
		public override string ToString()
		{
			var b = new StringBuilder("Battle\r\n");
			foreach (var ant in Orders.Keys)
			{
				b.AppendLine(ant + " " + Orders[ant]);
			}
			return b.ToString();
		}

		public void Add(Ant ant, Direction order, double aggression)
		{
			Orders.Add(ant, order);
			Aggression.Add(ant, aggression);
		}
	}

	public interface IBattleSplitter
	{
		IEnumerable<BattlePart> SplitIntoBattleParts();
	}

	public interface IBattlePartSolver
	{
		BattleSolution Solve(BattlePart battlePart, HashSet<Loc> antsUsedThisTurn);
	}

	public class BattleCommander
	{
		private readonly IGameState state;
		private readonly IBattleSplitter splitter;
		private readonly IBattlePartSolver solver;
		private readonly Func<bool> timeToFinishTurn;

		public BattleCommander(IGameState state, IBattleSplitter splitter, IBattlePartSolver solver, Func<bool> timeToFinishTurn)
		{
			this.state = state;
			this.splitter = splitter;
			this.solver = solver;
			this.timeToFinishTurn = timeToFinishTurn;
		}

		public void CommandAllBattles(HashSet<Loc> antsUsedThisTurn)
		{
			int battlesCount = 0;
			int antsCount = 0;
			foreach (var battlePart in splitter.SplitIntoBattleParts())
			{
				if (timeToFinishTurn()) break;
				var solution = solver.Solve(battlePart, antsUsedThisTurn);
				foreach (var order in solution.Orders)
				{
					state.IssueOrder(order.Key, order.Value, antsUsedThisTurn);
					V.MarkInBattleAnt(order.Key, solution.Aggression[order.Key]);
				}
				battlesCount++;
				antsCount += solution.Orders.Count;
				state.L("Battle {0}", solution);
			}
			state.L("BattlesCount {0} AntsCount {1}", battlesCount, antsCount);
		}
	}
}