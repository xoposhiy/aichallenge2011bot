using System;
using System.Collections.Generic;
using System.Linq;

namespace Ants
{
	public class Wave
	{
		public static void Run<T>(T first, Func<T, IEnumerable<T>> getNeighbours, Action<T> takeNext)
		{
			var q = new Queue<T>();
			var busy = new HashSet<T>();
			q.Enqueue(first);
			busy.Add(first);
			takeNext(first);
			while (q.Count > 0)
			{
				var a = q.Dequeue();
				var neighbours = getNeighbours(a);
				foreach (var n in neighbours.Where(item => !busy.Contains(item)))
				{
					q.Enqueue(n);
					busy.Add(n);
					takeNext(n);
				}
			}
		}

	}
}