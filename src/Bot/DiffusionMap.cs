using System;

namespace Ants
{
	public abstract class DiffusionMap
	{
		private double[,] map, newMap;
		private readonly double fadeoutRate = 0.5;
		private readonly double diffusionRate = 0.5;
		protected bool visualize = false;

		internal DiffusionMap(IGameState state, double fadeoutRate = 0.8, double diffusionRate = 1)
		{
			this.fadeoutRate = fadeoutRate;
			this.diffusionRate = diffusionRate;
			map = new double[state.Height, state.Width];
			newMap = new double[state.Height, state.Width];
		}

		public void Update(IGameState state)
		{
			for (int r = 0; r < state.Height; r++)
				for(int c = 0; c < state.Width; c++)
				{
					var value = CalculateSmell(state, new Loc(r, c));
					newMap[r, c] = value;
					if (visualize)
					{
						if (value > 0.0001 && (3*r+2*c) % 4 == 0)
						{
							var alfa = Math.Min(1, Math.Pow(value, 0.3));
							V.OverlaySetFillColor(255, 100, 100, alfa);
							V.OverlayRect(r - 0.5, c - 0.5, 1, 1, true);
						}
					}
				}
			var t = map;
			map = newMap;
			newMap = t;
		}

		public double this[Loc loc] { get { return map[loc.Row, loc.Col]; } }
		
		private double CalculateSmell(IGameState state, Loc loc)
		{
			if (state[loc] == Tile.Water) return 0;
			var v0 = this[loc];
			var sum = 0.0;
			var c = 0;
			for (int dr = -1; dr <= 1; dr++)
				for (int dc = -1; dc <= 1; dc++)
				{
					if (dr == 0 && dc == 0) continue;
					if (IsBlockedByWater(state, loc, dr, dc)) continue;
					c++;
					var delta = new Loc(dr, dc);
					var neighbour = state.GetDestination(loc, delta);
					sum += (this[neighbour] - v0);
				}
			if (c != 0) sum /= c;
			else sum = 0;
			sum *= diffusionRate;
			v0 *= fadeoutRate;
			return CalculateDiffusion(state, loc, sum + v0);
		}

		private static bool IsBlockedByWater(IGameState state, Loc loc, int dr, int dc)
		{
			return false;
//			if (dr == 0 || dc == 0) return false;
//			return
//				(dr == 1 && dc == 1 && state[state.GetDestination(loc, Direction.North)] == Tile.Water && state[state.GetDestination(loc, Direction.East)] == Tile.Water) ||
//				(dr == -1 && dc == 1 && state[state.GetDestination(loc, Direction.South)] == Tile.Water && state[state.GetDestination(loc, Direction.East)] == Tile.Water) ||
//				(dr == -1 && dc == -1 && state[state.GetDestination(loc, Direction.South)] == Tile.Water && state[state.GetDestination(loc, Direction.West)] == Tile.Water) ||
//				(dr == 1 && dc == -1 && state[state.GetDestination(loc, Direction.North)] == Tile.Water && state[state.GetDestination(loc, Direction.West)] == Tile.Water)
//				;
		}

		protected abstract double CalculateDiffusion(IGameState state, Loc loc, double newValue);
	}
}