using System;
using System.Collections.Generic;
using System.Linq;

namespace Ants
{
	public class BattlePartSolver : IBattlePartSolver
	{
		private readonly IGameState state;
		private readonly Loc hillToAttack;
		private readonly double aggression;

		public BattlePartSolver(IGameState state, Loc hillToAttack, double aggression = -1)
		{
			this.state = state;
			this.hillToAttack = hillToAttack;
			this.aggression = aggression;
		}

		public BattleSolution Solve(BattlePart battlePart, HashSet<Loc> antsUsedThisTurn)
		{
			var myAnts = battlePart.GetMyDynamic().ToArray();
			var agg = aggression >= 0 ? aggression : CalculateAgression(battlePart);
			var kutuzov = new Kutuzov2(battlePart.DynamicAnts.ToArray(), battlePart.StaticAnts.ToArray(), state, agg);
			var pos = kutuzov.MakePlan(10000);
			var battleSolution = new BattleSolution();
			for (int i = 0; i < myAnts.Length; i++)
				battleSolution.Add(myAnts[i], pos.Orders[i], aggression);
			return battleSolution;
		}

		private double CalculateAgression(BattlePart battle)
		{
			var aggr = new Func<BattlePart, double>[] {DefaultAggression, DefendMyHillAggression, SwarmAggression, HordesAggression}
				.Select(ag => ag(battle))
				.Max();
			state.L("A: " + aggr);
			return aggr;
		}

		public double DefaultAggression(BattlePart battle)
		{
			return 0.8;
		}

		public double HordesAggression(BattlePart battle)
		{
			var count = state.MyAnts.Count;
			if (count > 150) return 1.51;
			if (count > 130) return 1.11;
			if (count > 100) return 0.91;
			return 0;
		}

		public double DefendMyHillAggression(BattlePart battle)
		{
			if (state.MyHills.Any())
			{
				var distanceToMyHill = state.MyHills.Min(h => battle.DynamicAnts.Min(a => state.GetDistance(a, h)));
				if (distanceToMyHill < 2) return 1.12;
				if (distanceToMyHill < 10) return 0.9992;
			}
			return 0;
		}

		public double SwarmAggression(BattlePart battle)
		{
			if (battle.GetMyDynamic().Count() >= 2)
			{
				var anyMyAnt = battle.GetMyDynamic().Shuffle(state.R).First();
				if (state.MyAnts.Count(a => state.GetDistance(a, anyMyAnt) < 4) >= 6) return 1.103;
				if (state.MyAnts.Count(a => state.GetDistance(a, anyMyAnt) < 4) >= 4) return 0.9903;
			}
			return 0;
		}
	}
}