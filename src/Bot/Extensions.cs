using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ants
{
	public static class Extensions
	{

		public static IEnumerable<Loc> GetAllLocations(this IGameState state)
		{
			for (int r = 0; r < state.Height; r++)
				for (int c = 0; c < state.Width; c++)
				{
					yield return new Loc(r, c);
				}
		}

		public static void AddAll<T>(this ICollection<T> collection, params T[] itemsToAdd)
		{
			foreach (var item in itemsToAdd)
				collection.Add(item);
		}
		
		public static TV SafeGet<TK, TV>(this IDictionary<TK, TV> map, TK key, TV defaultValue = default(TV))
		{
			TV v;
			if (map.TryGetValue(key, out v)) return v;
			return defaultValue;
		}

		public static void MultiAdd<TK, TV>(this IDictionary<TK, List<TV>> map, TK key, TV value)
		{
			List<TV> vs;
			if (!map.TryGetValue(key, out vs)) map.Add(key, vs = new List<TV>());
			vs.Add(value);
		}

		public static string JoinStrings(this IEnumerable<string> items, string delimiter)
		{
			var ar = items.ToArray();
			if (ar.Length == 0) return "";
			var s = new StringBuilder();
			s.Append(ar[0]);
			for (int i = 1; i < ar.Length; i++)
				s.Append(delimiter + ar[i]);
			return s.ToString();
		}

		public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source, Random rng)
		{
			T[] elements = source.ToArray();
			for (int i = elements.Length - 1; i > 0; i--)
			{
				int swapIndex = rng.Next(i + 1);
				T tmp = elements[i];
				elements[i] = elements[swapIndex];
				elements[swapIndex] = tmp;
			}
			return elements;
		}

		public static HashSet<T> ToHashSet<T>(this IEnumerable<T> source)
		{
			var res = new HashSet<T>();
			foreach (var s in source) res.Add(s);
			return res;
		}

		public static T RandomOrDefault<T>(this IEnumerable<T> source, Random r)
		{
			var list = source.ToList();
			return list.Any() ? list[r.Next(list.Count)] : default(T);
		}

		public static Loc FindClosest(this IEnumerable<Loc> all, Loc loc, IGameState state)
		{
			return all.MinItem(f => state.GetDistance(loc, f));
		}

		public static Loc FindClosest(this IEnumerable<Loc> all, IEnumerable<Loc> locs, IGameState state)
		{
			return all.MinItem(f => locs.Min(loc => state.GetDistance(loc, f)));
		}

		public static int MinOrZero<T>(this IEnumerable<T> source, Func<T, int> get)
		{
			var min = int.MaxValue;
			var hasValue = false;
			foreach (var item in source)
			{
				if (get(item) < min) min = get(item);
				hasValue = true;
			}
			return hasValue ? min : 0;
		}

		public static T MaxItem<T>(this IEnumerable<T> source, Func<T, int> valueToMaxify)
		{
			return MinItem(source, arg => -valueToMaxify(arg));
		}

		public static T MaxItem<T>(this IEnumerable<T> source, Func<T, double> valueToMaxify)
		{
			return MinItem(source, arg => -valueToMaxify(arg));
		}

		public static T MinItem<T>(this IEnumerable<T> source, Func<T, int> valueToMinify)
		{
			int minV = int.MaxValue;
			T minItem = default(T);
			foreach (var item in source)
			{
				var v = valueToMinify(item);
				if (v < minV)
				{
					minItem = item;
					minV = v;
				}
			}
			return minItem;
		}

		public static T MinItem<T>(this IEnumerable<T> source, Func<T, double> valueToMinify)
		{
			double minV = int.MaxValue;
			T minItem = default(T);
			foreach (var item in source)
			{
				var v = valueToMinify(item);
				if (v < minV)
				{
					minItem = item;
					minV = v;
				}
			}
			return minItem;
		}
	}
}