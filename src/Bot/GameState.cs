using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Ants
{

	public class GameState : IGameState
	{
		private readonly bool suppressOutput;
		private readonly List<Loc> visibleHills = new List<Loc>();
		private readonly List<Loc> visibleFood = new List<Loc>();
		/// <summary>
		/// key - a location which will be explored on next turn
		/// value - ant(s) the location will be explored by
		/// </summary>
		private IDictionary<Loc, ICollection<Loc>> locationsExploredNextTurn = new Dictionary<Loc, ICollection<Loc>>();
		private IDictionary<Loc, ICollection<Loc>> newlyExploredLocations = null;

		public Random R { get; private set; }

		public int Width { get; private set; }
		public int Height { get; private set; }

		public int LoadTime { get; private set; }
		public long TurnTime { get; private set; }

		private Stopwatch turnStopwatch = new Stopwatch();
		public long TimeRemaining
		{
			get
			{
				var timeSpent = turnStopwatch.ElapsedMilliseconds;
				return TurnTime - timeSpent;
			}
		}

		public int ViewRadius2 { get; private set; }
		public int AttackRadius2 { get; private set; }
		public int SpawnRadius2 { get; private set; }

		public HashSet<Loc> MyAnts { get; private set; }
		public List<Loc> MyHills { get; private set; }
		public List<Loc> EnemyAnts { get; private set; }
		public List<Loc> EnemyHills { get; private set; }
		public List<Loc> DeadTiles { get; private set; }
		public HashSet<Loc> FoodTiles { get; private set; }
		public HashSet<Loc> SeenBorder { get; private set; }

		public int GetLastSeenTime(Loc loc)
		{
			return seen[loc.Row, loc.Col];
		}

		public IEnumerable<Direction> GetFreeDirections(Loc loc, HashSet<Loc> antsBusyInThisTurn)
		{
			return GetLandDirections(loc).Where(d => !antsBusyInThisTurn.Contains(GetDestination(loc, d)));
		}

		public long TimeSpent
		{
			get { return turnStopwatch.ElapsedMilliseconds; }
		}

		public int TurnNo { get; set; }

		public Tile this[Loc loc]
		{
			get
			{
				if (loc.Row < 0 || loc.Row >= Height || loc.Col < 0 || loc.Col >= Width)
					throw new Exception(loc.ToString());
				return map[loc.Row, loc.Col];
			}
		}

		public Tile this[int row, int col]
		{
			get { return map[row, col]; }
		}

		private readonly Tile[,] map;
		private readonly int[,] seen;	


		/// <summary>
		/// Contains newly explored locations per direction
		/// </summary>
		private IDictionary<Direction, IEnumerable<Loc>> NewlyExplored;
		private HashSet<Loc> ViewArea;


		public static GameState CreateTest(int width = 10, int height = 10, bool suppressOutput = false)
		{
			return new GameState(width, height, 100, 100, 77, 5, 1, 42, suppressOutput);
		}

		public GameState(int width, int height, int turntime, int loadtime, int viewradius2, int attackradius2, int spawnradius2, int seed, bool suppressOutput = false)
		{
			this.suppressOutput = suppressOutput;
			R = new Random(seed);
			TurnNo = 0;
			Width = width;
			Height = height;

			LoadTime = loadtime;
			TurnTime = turntime;

			ViewRadius2 = viewradius2;
			AttackRadius2 = attackradius2;
			SpawnRadius2 = spawnradius2;

			MyAnts = new HashSet<Loc>();
			MyHills = new List<Loc>();
			EnemyAnts = new List<Loc>();
			EnemyHills = new List<Loc>();
			DeadTiles = new List<Loc>();
			FoodTiles = new HashSet<Loc>();
			SeenBorder = new HashSet<Loc>();
			map = new Tile[height, width];
			seen = new int[height, width];
			for (int row = 0; row < height; row++)
			{
				for (int col = 0; col < width; col++)
				{
					map[row, col] = Tile.Land;
					seen[row, col] = int.MinValue;
				}
			}

			FillNewlyExplored();
		}

		private void FillNewlyExplored()
		{
			var zeroLoc = new Loc(0, 0);
			ViewArea = new HashSet<Loc>();

			for (int i = 0; i * i <= ViewRadius2; i++)
				for (int j = 0; j * j <= ViewRadius2; j++)
				{
					var currentLoc = new Loc(i, j);
					if (GetEqDistance2(zeroLoc, currentLoc) < ViewRadius2)
						ViewArea.Add(currentLoc);
				}

			ViewArea.UnionWith(ViewArea.Select(loc => new Loc(loc.Row, -loc.Col)).ToArray());
			ViewArea.UnionWith(ViewArea.Select(loc => new Loc(-loc.Row, loc.Col)).ToArray());

			NewlyExplored = new Dictionary<Direction, IEnumerable<Loc>>();

			foreach (var direction in Ants.Aim.Keys)
				NewlyExplored.Add(direction, ViewArea.Select(loc => loc.Add(Ants.Aim[direction])).Where(loc => !ViewArea.Contains(loc)).ToArray());
		}

		#region State mutators
		public void StartNewTurn()
		{
			// start timer
			turnStopwatch = Stopwatch.StartNew();

			// clear ant data
			foreach (var loc in MyAnts) map[loc.Row, loc.Col] = Tile.Land;
			foreach (var loc in EnemyAnts) map[loc.Row, loc.Col] = Tile.Land;
			foreach (var loc in DeadTiles) map[loc.Row, loc.Col] = Tile.Land;

			MyAnts.Clear();
			visibleHills.Clear();
			visibleFood.Clear();
			EnemyAnts.Clear();
			DeadTiles.Clear();

			newlyExploredLocations = locationsExploredNextTurn;
			locationsExploredNextTurn = new Dictionary<Loc, ICollection<Loc>>();
			TurnNo++;
		}

		public Ant AddAnt(int row, int col, int team)
		{
			map[row, col] = Tile.Ant;

			var ant = new Ant(row, col, team);
			if (team == 0)
			{
				MyAnts.Add(ant);
			}
			else
			{
				EnemyAnts.Add(ant);
			}
			return ant;
		}

		public void AddFood(int row, int col)
		{
			map[row, col] = Tile.Food;
			var food = new Loc(row, col);
			FoodTiles.Add(food);
			visibleFood.Add(food);
		}

		public void RemoveFood(int row, int col)
		{
			// an ant could move into a spot where a food just was
			// don't overwrite the space unless it is food
			if (map[row, col] == Tile.Food)
			{
				map[row, col] = Tile.Land;
			}
			FoodTiles.Remove(new Loc(row, col));
		}

		public void AddWater(int row, int col)
		{
			map[row, col] = Tile.Water;
		}

		public void DeadAnt(int row, int col)
		{
			// food could spawn on a spot where an ant just died
			// don't overwrite the space unless it is land
			if (map[row, col] == Tile.Land)
			{
				map[row, col] = Tile.Dead;
			}

			// but always add to the dead list
			DeadTiles.Add(new Loc(row, col));
		}

		public void AntHill(int row, int col, int team)
		{

			if (map[row, col] == Tile.Land)
			{
				map[row, col] = Tile.Hill;
			}

			var hill = new AntHill(row, col, team);
			if (team == 0)
			{
				if (!MyHills.Contains(hill))
				{
					MyHills.Add(hill);
					SetSeen(ViewArea.Select(loc => GetDestination(hill, loc)));
				}
			}
			else
			{
				if (!EnemyHills.Contains(hill)) EnemyHills.Add(hill);
			}
			visibleHills.Add(hill);
		}
		#endregion

		/// <summary>
		/// Gets whether <paramref name="loc"/> is passable or not.
		/// </summary>
		/// <param name="loc">The Loc to check.</param>
		/// <returns><c>true</c> if the Loc is not water, <c>false</c> otherwise.</returns>
		/// <seealso cref="IsUnoccupied(Loc)"/>
		public bool IsLand(Loc loc)
		{
			return map[loc.Row, loc.Col] != Tile.Water;
		}

		public bool IsLand(Loc loc, Direction dir)
		{
			return IsLand(GetDestination(loc, dir));
		}

		/// <summary>
		/// Gets whether <paramref name="loc"/> is occupied or not.
		/// </summary>
		/// <param name="loc">The Loc to check.</param>
		/// <returns><c>true</c> if the Loc is passable and does not contain an ant, <c>false</c> otherwise.</returns>
		public bool IsUnoccupied(Loc loc)
		{
			return IsLand(loc) && map[loc.Row, loc.Col] != Tile.Ant;
		}

		/// <summary>
		/// Gets the destination if an ant at <paramref name="loc"/> goes in <paramref name="direction"/>, accounting for wrap around.
		/// </summary>
		/// <param name="loc">The starting Loc.</param>
		/// <param name="direction">The direction to move.</param>
		/// <returns>The new Loc, accounting for wrap around.</returns>
		public Loc GetDestination(Loc loc, Direction direction)
		{
			if (direction == Direction.None) return loc;
			Loc delta = Ants.Aim[direction];
			return GetDestination(loc, delta);
		}

		public Loc GetDestination(Loc loc, Loc delta)
		{
			int row = (loc.Row + delta.Row) % Height;
			if (row < 0) row += Height; // because the modulo of a negative number is negative

			int col = (loc.Col + delta.Col) % Width;
			if (col < 0) col += Width;

			return new Loc(row, col);
		}

		/// <summary>
		/// Gets the distance between <paramref name="loc1"/> and <paramref name="loc2"/>.
		/// </summary>
		/// <param name="loc1">The first Loc to measure with.</param>
		/// <param name="loc2">The second Loc to measure with.</param>
		/// <returns>The distance between <paramref name="loc1"/> and <paramref name="loc2"/></returns>
		public int GetDistance(Loc loc1, Loc loc2)
		{
			return GetAbsDelta(loc1, loc2).ManhatLen();
		}

		public int GetEqDistance2(Loc loc1, Loc loc2)
		{
			return GetAbsDelta(loc1, loc2).Eq2Len();
		}

		public Loc GetAbsDelta(Loc loc1, Loc loc2)
		{
			int drow = Math.Abs(loc1.Row - loc2.Row);
			drow = Math.Min(drow, Height - drow);

			int dcol = Math.Abs(loc1.Col - loc2.Col);
			dcol = Math.Min(dcol, Width - dcol);

			return new Loc(drow, dcol);
		}

		public Loc GetDelta(Loc loc1, Loc loc2)
		{
			int dx1 = loc2.Row - loc1.Row;
			int dx2 = loc2.Row - loc1.Row + Height;
			int dx3 = loc2.Row - loc1.Row - Height;
			int dy1 = loc2.Col - loc1.Col;
			int dy2 = loc2.Col - loc1.Col + Width;
			int dy3 = loc2.Col - loc1.Col - Width;

			return new Loc(MinAbs(dx1, dx2, dx3), MinAbs(dy1, dy2, dy3));
		}

		private int MinAbs(int a, int b, int c)
		{
			if (Math.Abs(a) <= Math.Abs(b))
				return Math.Abs(a) <= Math.Abs(c) ? a : c;
			return Math.Abs(b) <= Math.Abs(c) ? b : c;

		}

		/// <summary>
		/// Gets the closest directions to get from <paramref name="loc1"/> to <paramref name="loc2"/>.
		/// </summary>
		/// <param name="loc1">The Loc to start from.</param>
		/// <param name="loc2">The Loc to determine directions towards.</param>
		/// <returns>The 1 or 2 closest directions from <paramref name="loc1"/> to <paramref name="loc2"/></returns>
		public ICollection<Direction> GetDirections(Loc loc1, Loc loc2)
		{
			var directions = new List<Direction>();

			if (loc1.Row < loc2.Row)
			{
				if (loc2.Row - loc1.Row >= Height / 2)
					directions.Add(Direction.North);
				if (loc2.Row - loc1.Row <= Height / 2)
					directions.Add(Direction.South);
			}
			if (loc2.Row < loc1.Row)
			{
				if (loc1.Row - loc2.Row >= Height / 2)
					directions.Add(Direction.South);
				if (loc1.Row - loc2.Row <= Height / 2)
					directions.Add(Direction.North);
			}

			if (loc1.Col < loc2.Col)
			{
				if (loc2.Col - loc1.Col >= Width / 2)
					directions.Add(Direction.West);
				if (loc2.Col - loc1.Col <= Width / 2)
					directions.Add(Direction.East);
			}
			if (loc2.Col < loc1.Col)
			{
				if (loc1.Col - loc2.Col >= Width / 2)
					directions.Add(Direction.East);
				if (loc1.Col - loc2.Col <= Width / 2)
					directions.Add(Direction.West);
			}

			return directions;
		}

		public bool IsInAttackRange(Loc loc1, Loc loc2)
		{
			return GetEqDistance2(loc1, loc2) <= AttackRadius2;
		}

		public bool IsInAttackRangeOnNextTurn(Loc loc1, Loc loc2)
		{
			var d = GetAbsDelta(loc1, loc2);
			var minDOnNextTurn = new[]
						  {
							  new Loc(d.Row - 2, d.Col), 
							  new Loc(d.Row - 1, d.Col - 1), 
							  new Loc(d.Row, d.Col - 2)
						  }
				.Min(l => l.Eq2Len());
			return minDOnNextTurn <= AttackRadius2;
		}

		public bool IsInAttackRadiusPlus1(Loc loc1, Loc loc2)
		{
			var d = GetAbsDelta(loc1, loc2);
			var minDOnNextTurn = new[]
						  {
							  new Loc(d.Row - 1, d.Col), 
							  new Loc(d.Row, d.Col - 1)
						  }
				.Min(l => l.Eq2Len());
			return minDOnNextTurn <= AttackRadius2;
		}

		public bool IsVisible(Loc loc)
		{
			return MyAnts.Any(a => GetEqDistance2(a, loc) < ViewRadius2);
			/*

						var offsets = new List<Loc>();
						var squares = (int)Math.Floor(Math.Sqrt(ViewRadius2));
						for (int r = -1 * squares; r <= squares; ++r)
						{
							for (int c = -1 * squares; c <= squares; ++c)
							{
								int square = r * r + c * c;
								if (square < ViewRadius2)
								{
									offsets.Add(new Loc(r, c));
								}
							}
						}
						foreach (Ant ant in MyAnts)
						{
							foreach (Loc offset in offsets)
							{
								if ((ant.Col + offset.Col) == loc.Col &&
									(ant.Row + offset.Row) == loc.Row)
								{
											 return true;
								}
							}
						}
						return false;
			*/
		}

		public IEnumerable<Direction> GetPossibleFirstMove(Loc loc1, Loc loc2)
		{
			var dd = new Dictionary<Loc, int>();
			var q = new Queue<Loc>();
			q.Enqueue(loc2);
			dd.Add(loc2, 0);
			for (int i = 0; i < 500; i++)
			{
				if (q.Count() == 0) break;
				var loc = q.Dequeue();
				if (loc.Equals(loc1))
				{
					foreach (var dir in GetLandDirections(loc1))
					{
						var firstMove = GetDestination(loc1, dir);
						if (dd.ContainsKey(firstMove) && dd[firstMove] < dd[loc1])
							yield return dir;
					}
					yield break;
				}


				foreach (var dir in GetLandDirections(loc))
				{
					var destination = GetDestination(loc, dir);
					if (!dd.ContainsKey(destination))
					{
						q.Enqueue(destination);
						dd[destination] = dd[loc] + 1;
					}
				}
			}
			yield break;
		}

		public Loc IssueOrder(Loc myAnt, Direction direction, HashSet<Loc> antsBusyInThisTurn)
		{
			var newAnt = myAnt;
			var dest = GetDestination(myAnt, direction);
			if (direction != Direction.None && IsUnoccupied(dest))
			{
				if (!suppressOutput)
					Console.Out.WriteLine("o {0} {1} {2}", myAnt.Row, myAnt.Col, direction.ToChar());
				this.L("{0} goto {1}", myAnt, direction);
				MyAnts.Remove(new Ant(myAnt, 0));
				map[myAnt.Row, myAnt.Col] = Tile.Land;
				MyAnts.Add(new Ant(dest, 0));
				map[dest.Row, dest.Col] = Tile.Ant;
				newAnt = dest;

				foreach (var newlyExploredLocation in NewlyExplored[direction].Select(loc => GetDestination(myAnt, loc)))
				{
					if (!locationsExploredNextTurn.ContainsKey(newlyExploredLocation))
						locationsExploredNextTurn.Add(newlyExploredLocation, new List<Loc>() { newAnt });
					else
						locationsExploredNextTurn[newlyExploredLocation].Add(newAnt);
				}
			}
			antsBusyInThisTurn.Add(newAnt);
			return newAnt;
		}

		public IEnumerable<Direction> GetLandDirections(Loc loc)
		{
			return Ants.Aim.Keys.Where(dir => IsLand(GetDestination(loc, dir)));
		}

		public Loc Normalize(Loc loc)
		{
			return new Loc((loc.Row + Height) % Height, (loc.Col + Width) % Width);
		}

		/*NotNull*/
		/// <summary>
		/// will modify ants and possibly targets
		/// </summary>
		public List<Path> FindNonOverlappingPaths(HashSet<Loc> targets, HashSet<Loc> ants, bool removeTargets, Func<bool> shouldBreak, Func<Loc, IEnumerable<Direction>> getAlowedDirections, int maxIterationsCount = 20000)
		{
			int stepsDone;
			return FindNonOverlappingPaths(targets, ants, removeTargets, shouldBreak, getAlowedDirections, maxIterationsCount, out stepsDone);
		}

		private List<Path> FindNonOverlappingPaths(HashSet<Loc> targets, HashSet<Loc> ants, bool removeTargets, Func<bool> shouldBreak, Func<Loc, IEnumerable<Direction>> getAlowedDirections, int maxIterationsCount, out int itersDone)
		{
			var paths = new List<Path>();
			var q = new Queue<PathFinderQueueItem>(targets.Select(t => new PathFinderQueueItem(t, t)));
			var visitedLocs = new HashSet<Loc>(targets);
			var field = new Dictionary<Loc, PathItem>();
			for (itersDone = 0; itersDone < maxIterationsCount; itersDone++)
			{
				if (q.Count == 0 || ants.Count == 0 || shouldBreak()) break;
				var queueItem = q.Dequeue();
				if (!targets.Contains(queueItem.Target)) continue; //obsolete queue item
				if (ants.Contains(queueItem.Loc))
				{
					var path = new Path(queueItem.Loc, queueItem.Target, new List<PathItem>());
					for (var loc = queueItem.Loc; loc != queueItem.Target; loc = field[loc].To)
						path.PathItems.Add(field[loc]);
					paths.Add(path);
					ants.Remove(queueItem.Loc);
					if (removeTargets) targets.Remove(queueItem.Target);
					continue;
				}
				foreach (var dir in getAlowedDirections(queueItem.Loc).Shuffle(R))
				{
					var nextLoc = GetDestination(queueItem.Loc, dir);
					if (!visitedLocs.Contains(nextLoc))
					{
						q.Enqueue(new PathFinderQueueItem(nextLoc, queueItem.Target));
						visitedLocs.Add(nextLoc);
						field[nextLoc] = new PathItem(nextLoc, queueItem.Loc, dir.Opposite());
					}
				}
			}
			return paths;
		}

		public void RemoveObsoleteData()
		{
			foreach (var hill in EnemyHills.ToArray())
				if (IsVisible(hill) && !visibleHills.Contains(hill)) EnemyHills.Remove(hill);
			foreach (var hill in MyHills.ToArray())
				if (IsVisible(hill) && !visibleHills.Contains(hill)) MyHills.Remove(hill);
			foreach (var food in FoodTiles.ToArray())
				if (IsVisible(food) && !visibleFood.Contains(food)) RemoveFood(food.Row, food.Col);
		}

		public void UpdateSeenArea()
		{
			SetSeen(newlyExploredLocations.Where(kvp => kvp.Value.Any(loc => MyAnts.Contains(loc))).Select(kvp => kvp.Key));
		}

		private void SetSeen(IEnumerable<Loc> newlyExplored)
		{
			var start = DateTime.Now;
			var locationsUnseenBefore = new List<Loc>(newlyExploredLocations.Count);

			foreach (Loc newlyExploredLocation in newlyExplored)
				if (!IsSeen(newlyExploredLocation))
				{
					seen[newlyExploredLocation.Row, newlyExploredLocation.Col] = TurnNo;
					locationsUnseenBefore.Add(newlyExploredLocation);
				}

			SeenBorder.ExceptWith(locationsUnseenBefore);
			SeenBorder.UnionWith(locationsUnseenBefore
								.SelectMany(loc =>
									Ants.Aim.Keys.Select(direction => GetDestination(loc, direction)))
								.Where(neighbour => !IsSeen(neighbour))
							  );

			//this.L("{0} tiles seen, seen border length = {1} tiles, update duration: {2:0} ms", seen.Cast<bool>().Where(isSeen => isSeen).Count(), SeenBorder.Count, (DateTime.Now - start).TotalMilliseconds);
		}

		public bool IsSeen(Loc loc)
		{
			return seen[loc.Row, loc.Col] >= 0;
		}

		public bool IsUnoccupied(Loc loc, Direction direction)
		{
			return IsUnoccupied(GetDestination(loc, direction));
		}

		public void StopTimer()
		{
			turnStopwatch.Stop();
		}

		public void RemoveAnt(Loc ant)
		{
			map[ant.Row, ant.Col] = Tile.Land;
			EnemyAnts.Remove(ant);
		}
	}
}
