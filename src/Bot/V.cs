using System;
using System.Collections.Generic;

namespace Ants
{
	public static class V
	{
		public static bool Enable  = false;

		public static void OverlaySetLineWidth(int w)
		{
			if (Enable) Console.WriteLine("v setLineWidth {0}", w);
		}

		public static void OverlaySetLineColor(int r, int g, int b, double a)
		{
			if (Enable) Console.WriteLine("v setLineColor {0} {1} {2} {3}", r, g, b, a);
		}

		public static void OverlaySetFillColor(int r, int g, int b, double a)
		{
			if (Enable) Console.WriteLine("v setFillColor {0} {1} {2} {3}", r, g, b, a);
		}

		public static void OverlayCircle(double row, double col, double radius, bool fill)
		{
			if (Enable) Console.WriteLine("v circle {0} {1} {2} {3}", row, col, radius, fill);
		}

		public static void OverlayRect(double row, double col, double w, double h, bool fill)
		{
			if (Enable) Console.WriteLine("v rect {0} {1} {2} {3} {4}", row, col, w, h, fill);
		}

		public static void OverlayLine(double row1, double col1, double row2, double col2)
		{
			if (Enable) Console.WriteLine("v line {0} {1} {2} {3}", row1, col1, row2, col2);
		}
		
		public static void OverlayArrow(double row1, double col1, double row2, double col2)
		{
			if (Enable) Console.WriteLine("v arrow {0} {1} {2} {3}", row1, col1, row2, col2);
		}

		public static void MarkAttackingEnemyHill(Loc enemyHill)
		{
			if (Enable)
			{
				OverlaySetLineColor(255, 0, 0, 0.5);
				OverlaySetLineWidth(6);
				OverlayCircle(enemyHill.Row, enemyHill.Col, 4, false);
				OverlayCircle(enemyHill.Row, enemyHill.Col, 2, false);
				OverlaySetLineWidth(1);
			}
		}

		public static void MarkFoodPath(IEnumerable<PathItem> path, bool isClose = false)
		{
			MarkPath(path, 255, 255, isClose ? 255 : 0, isClose ? 1 : 0.4);
		}

		public static void MarkAttackingPath(IEnumerable<PathItem> path)
		{
			MarkPath(path, 255, 50, 0, 0.5);
		}

		public static void MarkSeenPath(IEnumerable<PathItem> path)
		{
			MarkPath(path, 20, 255, 20, 0.5);
		}

		public static void MarkPath(IEnumerable<PathItem> path, int r, int g, int b, double alpha)
		{
			if (!Enable) return;
			V.OverlaySetLineColor(r, g, b, alpha);
			foreach (var item in path)
			{
				if (Math.Abs(item.From.Row - item.To.Row) + Math.Abs(item.From.Col - item.To.Col) == 1)
					V.OverlayLine(item.From.Row, item.From.Col, item.To.Row, item.To.Col);
			}
		}

		public static void MarkForceDirection(Loc ant, Vector force, uint color = 0x00ff80)
		{
			if (!Enable) return;
			OverlaySetLineColor(R(color), G(color), B(color), A(color));
			var module = force.GetModule();
			OverlayArrow(ant.Row, ant.Col, ant.Row + 2 * force.Fy / module, ant.Col + 2 * force.Fx / module);
		}

		private const int ARGBAlphaShift = 24;
		private const int ARGBRedShift = 16;
		private const int ARGBGreenShift = 8;
		private const int ARGBBlueShift = 0;

		private static int R(uint color)
		{
			return (byte)((color >> ARGBRedShift) & 0xFF);
		}

		private static int G(uint color)
		{
			return (byte)((color >> ARGBGreenShift) & 0xFF);
		}

		private static int B(uint color)
		{
			return (byte)((color >> ARGBBlueShift) & 0xFF);
		}

		private static double A(uint color)
		{
			return (double)((color >> ARGBAlphaShift) & 0xFF) / 255;
		}

		public static void MarkUnseenBorder(HashSet<Loc> seenBorder)
		{
			if (!Enable) return;
			foreach (var loc in seenBorder)
			{
				OverlaySetFillColor(0, 0, 0, 0.1);
				OverlayCircle(loc.Row, loc.Col, 0.5, true);
			}
		}

		public static void MarkInBattleAnt(Loc myAnt, double agression)
		{
			if (!Enable) return;
			OverlaySetLineColor(255, 0, 0, 0.8);
			OverlaySetLineWidth(3);
			OverlayCircle(myAnt.Row, myAnt.Col, 0.8, false);
			OverlaySetLineWidth(1);
		}

		public static void MarkAnt(Loc myAnt, int r, int g, int b, int width=3)
		{
			if (!Enable) return;
			OverlaySetLineColor(r,g,b, 0.7);
			OverlaySetLineWidth(3);
			OverlayCircle(myAnt.Row, myAnt.Col, 0.8, false);
			OverlaySetLineWidth(1);
		}

		private static void MarkAntGroup(List<Loc> ants)
		{
			OverlaySetLineColor(255, 0, 0, 0.8);
			for(int i=0; i<ants.Count; i++)
			{
				var ant = ants[i];
				var next = ants[(i + 1)%ants.Count];
				OverlayLine(ant.Row, ant.Col, next.Row, next.Col);
			}
		}

		public static void MarkTile(Loc tile, int r, int g, int b)
		{
			if (!Enable) return;
			OverlaySetFillColor(r, g, b, 0.5);
			OverlayCircle(tile.Row, tile.Col, 0.5, true);
		}
	}
}