using System;
using System.Collections.Generic;
using System.Linq;

namespace Ants
{
	public partial class MyBot
	{
		private void DoGuardHills(HashSet<Loc> antsBusyInThisTurn)
		{
			var hillsCount = State.MyHills.Count;
			if (hillsCount == 0) return; // So sad :(
			var guardsCount = State.MyAnts.Count / 10 / hillsCount;
			foreach (var hill in State.MyHills)
			{
				var freeAnts = GetAntsNotBusyInThisTurn(antsBusyInThisTurn);
				var guards = FindClosestAnts(freeAnts, hill, guardsCount).ToArray();
				var defendRadius = CalculateDefendRadius(hill, guards);
				GuardHill(hill, guards.ToArray(), antsBusyInThisTurn, defendRadius);
				if (TimeToFinishTurn) return;
			}
		}

		private double CalculateDefendRadius(Loc hill, Loc[] defenders)
		{
			var landTiles = 0;
			for (var r = -3; r <= 3; r++)
				for (var c = -3; c <= 3; c++)
				{
					var delta = new Loc(r, c);
					var tile = State[State.GetDestination(hill, delta)];
					landTiles += tile == Tile.Land || tile == Tile.Hill ? 1 : 0;
				}
			var landDensity = landTiles / 49.0;
			return 1 + defenders.Length / (4 * landDensity);
		}

		private void GuardHill(Loc hill, Loc[] defenders, HashSet<Loc> antsBusyInThisTurn, double defendRadius)
		{
			if (TimeToFinishTurn) return;
			V.OverlaySetLineColor(0, 0, 255, 1);
			V.OverlayCircle(hill.Row, hill.Col, defendRadius, false);
			foreach (var ant in defenders)
			{
				var directions = CalculateGuardRepulsionForce(ant, hill, defendRadius);
//				State.L("Repulsion directions: " + directions.Aggregate("", (s, d) => s + " " + d.ToString()));
				RunInDirection(ant, directions, antsBusyInThisTurn);
				if (TimeToFinishTurn) return;
			}
		}

		private Direction[] CalculateGuardRepulsionForce(Loc ant, Loc hill, double defendRadius)
		{
//			State.L("Guard hill {0} by {1}", hill, ant);
			if (ant.Equals(hill))
			{
				// ��������� � �����������, ����� �� ������������ ����.
				return new[] { Direction.East, Direction.North, Direction.South, Direction.West }.Shuffle(State.R).ToArray();
			}
			if (State.GetDistance(hill, ant) == 1)
			{
				// ��������� � ������� �� �����������, ����� ���������� ������ ������ ��� ������������.
				var dirs = State.GetDirections(hill, ant).ToArray();
				return dirs;
			}
			var dHill = State.GetDelta(ant, hill);
			double dHillLen = dHill.ManhatLen();
			var sign = dHillLen <= defendRadius ? -1 : 1;
			var force = new Vector(0.5 * sign * dHill.Col / dHillLen, 0.5 * sign * dHill.Row / dHillLen);
			force = force.Add(State.MyAnts.AttractionR2Force(ant, -1, State));
			V.MarkForceDirection(ant, force, 0x0000ff80);
//			State.L("Guard force: " + force);
			return force.ToDirections();
		}
	}
}