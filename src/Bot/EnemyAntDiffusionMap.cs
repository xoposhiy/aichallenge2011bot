namespace Ants
{
	public class EnemyAntDiffusionMap : DiffusionMap
	{
		public EnemyAntDiffusionMap(IGameState state)
			: base(state)
		{
			visualize = false;
		}

		protected override double CalculateDiffusion(IGameState state, Loc loc, double newValue)
		{
			if (state[loc] == Tile.Ant)
			{
				if (state.EnemyAnts.Contains(loc)) return 1;
				else return newValue*0.8;
			}
			return newValue;
		}
	}
}