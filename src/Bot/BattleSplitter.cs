using System.Collections.Generic;
using System.Linq;

namespace Ants
{
	public class BattleSplitter : IBattleSplitter
	{
		private readonly IGameState state;
		private readonly HashSet<Loc> antsUsedThisTurn;
		private readonly int maxDynamicAntsCount;
		private readonly HashSet<Ant> usedAsDynamicAlready = new HashSet<Ant>();

		public BattleSplitter(IGameState state, HashSet<Loc> antsUsedThisTurn, int maxDynamicAntsCount = 6)
		{
			this.state = state;
			this.antsUsedThisTurn = antsUsedThisTurn;
			this.maxDynamicAntsCount = maxDynamicAntsCount;
		}

		public IEnumerable<BattlePart> SplitIntoBattleParts()
		{
			return 
				state.MyAnts.ToList()
					.Where(ant => state.MyAnts.Contains(ant) && !antsUsedThisTurn.Contains(ant) && !usedAsDynamicAlready.Contains((Ant)ant))
					.Select((Loc loc) => FindBattlePart(loc))
					.Where(part => part.AllEnemyAnts().Any());
		}

		private BattlePart FindBattlePart(Loc loc)
		{
			var battle = new BattlePart(maxDynamicAntsCount, state, antsUsedThisTurn);
			Wave.Run((Ant) loc, e => GetEnemyNeighbours(e, battle), battle.AddAnt);
			if (battle.AllEnemyAnts().Any())
				foreach (var ant in battle.DynamicAnts) usedAsDynamicAlready.Add(ant);
			return battle;
		}

		public IEnumerable<Ant> GetEnemyNeighbours(Ant ant, BattlePart battle)
		{
			// ���� ant - � battle.Static, �� ��� ������� ��� �� ����!
			// ���� ���� ������� (� Dynamic), �� ���� ������ �� ���������� ar+2.
			// ���� ��� ����� �������, �� 
			//		����� ���� �����, �� ��������� �� ���������� ar+2 - ��� ����� ������! 
			//		�������� ����� ��������� �� ���������� ar+1
			//		��������� ����� �� ������ ������ �� �����. ��� ����� ������� � �� �������� �� �����.
			
			if (battle.IsInStatic(ant)) return new Ant[0];
			if (ant.Team == 0)
				return state.EnemyAnts.Cast<Ant>().Where(e => state.IsInAttackRangeOnNextTurn(ant, e) && !battle.Contains(e));
			return state.MyAnts.Cast<Ant>()
				.Where(my => antsUsedThisTurn.Contains(my) ? state.IsInAttackRadiusPlus1(my, ant) : state.IsInAttackRangeOnNextTurn(my, ant))
				.Where(my => !battle.Contains(my));
		}
	}
}