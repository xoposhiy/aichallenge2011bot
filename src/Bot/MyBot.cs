using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Ants
{
	partial class MyBot
	{
		public static void Main(string[] args)
		{
			new Ants().PlayGame(new MyBot());
		}

		public override void DoTurn()
		{
			V.MarkUnseenBorder(State.SeenBorder);
			var antsBusyInThisTurn = new HashSet<Loc>();
			var engagedCloseFood = new HashSet<Loc>();
			Stage("GatherFood", () => DoGatherFood(antsBusyInThisTurn, engagedCloseFood));
			Stage("Battles", () => DoBattles(antsBusyInThisTurn));
			Stage("Guard", () => DoGuardHills(antsBusyInThisTurn));
			Stage("Attack", () => DoAttackEnemyHill(antsBusyInThisTurn));
			Stage("Support", () => DoSupportAttackers(antsBusyInThisTurn));
			Stage("Visible", () => DoKeepMapVisible(antsBusyInThisTurn));
			Stage("Border", () => DoGoToBorder(antsBusyInThisTurn));
			Stage("Spread", () => DoSpread(antsBusyInThisTurn));
		}


		private void Stage(string stageName, Action stage)
		{
			if (TimeToFinishTurn)
			{
				State.L("{0} skipped - no time!", stageName);
				return;
			}

			var sw = Stopwatch.StartNew();

			stage();
	
			sw.Stop();
			var elapsed = sw.ElapsedMilliseconds;
			stats[stageName] = stats.ContainsKey(stageName) ? new StageTimeStats(stats[stageName], elapsed) : new StageTimeStats(elapsed);
			State.L("PERFORMANCE {0} {1} {2}, time left {3}", stageName, elapsed, stats[stageName], State.TimeRemaining);
		}

		private readonly IDictionary<string, StageTimeStats> stats = new Dictionary<string, StageTimeStats>();


		private bool TimeToFinishTurn
		{
			get
			{
				var timeIsRunningOut = State.TimeRemaining < 50;
				if (timeIsRunningOut) State.L("We are running short of time! Time remaining: {0} ms", State.TimeRemaining);
				return timeIsRunningOut;
			}
		}

		private HashSet<Loc> GetAntsNotBusyInThisTurn(HashSet<Loc> antsBusyInThisTurn)
		{
			return new HashSet<Loc>(State.MyAnts.Except(antsBusyInThisTurn));
		}

		private IEnumerable<Loc> FindClosestAnts(IEnumerable<Loc> ants, Loc target, int count)
		{
			if (count == 1)
			{
				Loc ant = ants.MinItem(a => State.GetDistance(a, target));
				return ant != null ? new[] { ant } : new Loc[0];
			}
			return ants.OrderBy(a => State.GetDistance(a, target)).Take(count).ToArray();
		}

		private bool MoveIsDangerous(Loc ant, Direction dir)
		{
			var newAntLoc = State.GetDestination(ant, dir);
			switch (dir)
			{
				case Direction.None:
					return false;
				case Direction.North:
					return MoveIsDangerous(newAntLoc, -2, 1, -2, 2);
				case Direction.South:
					return MoveIsDangerous(newAntLoc, -1, 2, -2, 2);
				case Direction.East:
					return MoveIsDangerous(newAntLoc, -2, 2, -1, 2);
				case Direction.West:
					return MoveIsDangerous(newAntLoc, -2, 2, -2, 1);
			}
			return false;
		}

		private bool MoveIsDangerous(Loc newAntLoc, int rowLBound, int rowRBound, int colLBound, int colRBound)
		{
			for (var r = rowLBound; r <= rowRBound; r++)
				for (var c = colLBound; c <= colRBound; c++)
				{
					var delta = new Loc(r, c);
					var loc = State.GetDestination(newAntLoc, delta);
					if (State[loc] == Tile.Ant && !State.MyAnts.Contains(loc)) return true;
				}
			return false;
		}
	}
}