using System;
using System.Collections.Generic;
using System.Linq;

namespace Ants
{
	public class AttackersProvider
	{
		public readonly Ant[] Ants;
		private readonly Dictionary<Ant, Ant[]> cache = new Dictionary<Ant, Ant[]>();
		private readonly IGameState state;

		public AttackersProvider(Ant[] ants, IGameState state)
		{
			Ants = ants;
			this.state = state;
		}

		public Ant[] GetAttackersOf(Ant ant)
		{
			Ant[] res;
			if (!cache.TryGetValue(ant, out res))
				cache.Add(ant, res = CalculateFocus(ant));
			return res;
		}

		public static int[] GetDeadsByPlayerIndex(AttackersProvider[] attackersProviders)
		{
			var res = new int[attackersProviders.Length];
			for(int i=0; i<res.Length;i++)
				res[i] = attackersProviders[i].Ants.Count(ant => IsDead(ant, attackersProviders, i));
			return res;
		}

		private static bool IsDead(Ant ant, AttackersProvider[] allAttackers, int meIndex)
		{
			var attackers = allAttackers.Where((provider, i) => i != meIndex).SelectMany(attackerProvider => attackerProvider.GetAttackersOf(ant)).ToList();
			return attackers.Any(a => allAttackers[meIndex].GetAttackersOf(a).Length <= attackers.Count);
		}


		private Ant[] CalculateFocus(Ant ant)
		{
			return Ants.Where(a => state.IsInAttackRange(a, ant)).ToArray();
		}
	}
	
	public class PlayerPositions
	{
		private readonly AttackersProvider attacks;
		
		public PlayerPositions(Ant[] initialLoc, Direction[] orders, IGameState state)
		{
			if (orders.Length != initialLoc.Length) throw new Exception(orders.Length + " != " + initialLoc.Length);
			Orders = (Direction[]) orders.Clone();
			InitialLocs = initialLoc;
			FinalLocs = initialLoc.Select((ant, i) => new Ant(state.GetDestination(ant, orders[i]), InitialLocs[i].Team)).ToArray();
			attacks = new AttackersProvider(FinalLocs, state);
		}

		public readonly Direction[] Orders;
		public readonly Ant[] InitialLocs;
		public readonly Ant[] FinalLocs;
		public IEnumerable<KeyValuePair<Loc, Direction>> GetAll()
		{
			return Orders.Select((t, i) => new KeyValuePair<Loc, Direction>(InitialLocs[i], t));
		}

		public Ant[] GetAttackers(Ant ant)
		{
			return attacks.GetAttackersOf(ant);
		}

		public override string ToString()
		{
			string s = "";
			foreach (var o in Orders) s += o + " ";
			return s;
		}
	}
}