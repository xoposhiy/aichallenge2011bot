using System;
using System.Collections.Generic;

namespace Ants
{
	public interface IGameState
	{
		Random R { get; }

		/// <summary>
		/// Gets the width of the map.
		/// </summary>
		int Width { get; }

		/// <summary>
		/// Gets the height of the map.
		/// </summary>
		int Height { get; }

		/// <summary>
		/// Gets the allowed load time in milliseconds.
		/// </summary>
		int LoadTime { get; }

		/// <summary>
		/// Gets the allowed turn time in milliseconds.
		/// </summary>
		long TurnTime { get; }

		/// <summary>
		/// Gets the allowed turn time remaining in milliseconds.
		/// </summary>
		long TimeRemaining { get; }

		/// <summary>
		/// Gets the ant view range radius squared.
		/// </summary>
		int ViewRadius2 { get; }

		/// <summary>
		/// Gets the ant attack range radius squared.
		/// </summary>
		int AttackRadius2 { get; }

		/// <summary>
		/// Gets the ant hill spawn radius squared.
		/// </summary>
		int SpawnRadius2 { get; }

		/// <summary>
		/// Gets a list of your currently visible hills.
		/// </summary>
		List<Loc> MyHills { get; }

		/// <summary>
		/// Gets a list of your ants.
		/// </summary>
		HashSet<Loc> MyAnts { get; }

		/// <summary>
		/// Gets a list of currently visible enemy ants.
		/// </summary>
		List<Loc> EnemyAnts { get; }

		/// <summary>
		/// Gets a list of currently visible enemy hills.
		/// </summary>
		List<Loc> EnemyHills { get; }

		/// <summary>
		/// Gets a list of currently-visible locations where ants died last turn.
		/// </summary>
		List<Loc> DeadTiles { get; }

		/// <summary>
		/// Gets a list of food tiles visible this turn.
		/// </summary>
		HashSet<Loc> FoodTiles { get; }

		int TurnNo { get; }
		HashSet<Loc> SeenBorder { get; }
		long TimeSpent { get; }

		/// <summary>
		/// Gets the <see cref="Tile"/> for the <paramref name="loc"/>.
		/// </summary>
		Tile this[Loc loc] { get; }

		/// <summary>
		/// Gets the <see cref="Tile"/> for the <paramref name="row"/> and <paramref name="col"/>.
		/// </summary>
		Tile this[int row, int col] { get; }

		/// <summary>
		/// Gets whether <paramref name="loc"/> is passable or not.
		/// </summary>
		/// <param name="loc">The location to check.</param>
		/// <returns><c>true</c> if the location is not water, <c>false</c> otherwise.</returns>
		/// <seealso cref="GameState.IsUnoccupied"/>
		bool IsLand(Loc loc);
		bool IsLand(Loc loc, Direction dir);

		/// <summary>
		/// Gets whether <paramref name="loc"/> is occupied or not.
		/// </summary>
		/// <param name="loc">The location to check.</param>
		/// <returns><c>true</c> if the location is passable and does not contain an ant, <c>false</c> otherwise.</returns>
		bool IsUnoccupied(Loc loc);

		bool IsUnoccupied(Loc loc, Direction direction);

		/// <summary>
		/// Gets the destination if an ant at <paramref name="loc"/> goes in <paramref name="direction"/>, accounting for wrap around.
		/// </summary>
		/// <param name="loc">The starting location.</param>
		/// <param name="direction">The direction to move.</param>
		/// <returns>The new location, accounting for wrap around.</returns>
		Loc GetDestination(Loc loc, Direction direction);

		Loc GetDestination(Loc loc, Loc delta);

		/// <summary>
		/// Gets the distance between <paramref name="loc1"/> and <paramref name="loc2"/>.
		/// </summary>
		/// <param name="loc1">The first location to measure with.</param>
		/// <param name="loc2">The second location to measure with.</param>
		/// <returns>The distance between <paramref name="loc1"/> and <paramref name="loc2"/></returns>
		int GetDistance(Loc loc1, Loc loc2);

		/// <summary>
		/// Gets the closest directions to get from <paramref name="loc1"/> to <paramref name="loc2"/>.
		/// </summary>
		/// <param name="loc1">The location to start from.</param>
		/// <param name="loc2">The location to determine directions towards.</param>
		/// <returns>The 1 or 2 closest directions from <paramref name="loc1"/> to <paramref name="loc2"/></returns>
		ICollection<Direction> GetDirections(Loc loc1, Loc loc2);

		bool IsVisible(Loc loc);
		IEnumerable<Direction> GetPossibleFirstMove(Loc loc1, Loc loc2);
		Loc IssueOrder(Loc myAnt, Direction direction, HashSet<Loc> antsBusyInThisTurn);
		int GetEqDistance2(Loc loc1, Loc loc2);
		Loc GetAbsDelta(Loc loc1, Loc loc2);
		bool IsInAttackRange(Loc loc1, Loc loc2);
		bool IsInAttackRangeOnNextTurn(Loc loc1, Loc loc2);
		IEnumerable<Direction> GetLandDirections(Loc loc);

		List<Path> FindNonOverlappingPaths(
			HashSet<Loc> targets, HashSet<Loc> ants, bool removeTargets, Func<bool> shouldBreak,
			Func<Loc, IEnumerable<Direction>> getAlowedDirections,
			int maxIterationsCount = 10000);

		Loc Normalize(Loc loc);
		Loc GetDelta(Loc loc2, Loc loc1);
		bool IsInAttackRadiusPlus1(Loc loc1, Loc loc2);
		int GetLastSeenTime(Loc loc);
		IEnumerable<Direction> GetFreeDirections(Loc loc, HashSet<Loc> antsBusyInThisTurn);
	}
}