using System;
using System.Collections.Generic;
using System.Linq;

namespace Ants
{
	public partial class MyBot
	{
		private IEnumerable<Loc> GetTilesToScout()
		{
			for (int r = 0; r < State.Height; r++)
				for (int c = 0; c < State.Width; c++)
				{
					var l = new Loc(r, c);
					if (State.GetLastSeenTime(l) >= State.TurnNo - 50)
						yield return l;
				}
		} 

		private void DoKeepMapVisible(HashSet<Loc> antsBusyInThisTurn)
		{
			var freeAnts = GetAntsNotBusyInThisTurn(antsBusyInThisTurn);
			var stepSize = (int)(Math.Sqrt(State.ViewRadius2)*0.5);
			var targetsToScout = new HashSet<Loc>();
			for(int r=0; r<State.Height; r+=stepSize)
				for(int c=0; c<State.Width; c+=stepSize)
				{
					var loc = new Loc(r, c);	
					if (State.GetLastSeenTime(loc) > 0 &&  !State.IsVisible(loc) && State.IsLand(loc))
					{
						targetsToScout.Add(loc);
						V.MarkTile(loc, 0, 255, 0);
					}
				}


			var paths = State.FindNonOverlappingPaths(
				targetsToScout,
				freeAnts, 
				true,
				() => targetsToScout.Count == 0 || TimeToFinishTurn,
				loc => State.GetFreeDirections(loc, antsBusyInThisTurn));
			foreach (Path path in paths)
			{
				if (path != null && path.PathItems.Count > 0)
				{
					V.MarkPath(path.PathItems, 0, 255, 255, 0.5);
					State.IssueOrder(path.Ant, path.PathItems[0].Direction, antsBusyInThisTurn);
				}
			}
		}


		private void DoSpread(HashSet<Loc> antsBusyInThisTurn)
		{
			foreach (var ant in GetAntsNotBusyInThisTurn(antsBusyInThisTurn))
			{
				RunInDirection(ant, CalculateRepulsionForce(ant), antsBusyInThisTurn);
				if (TimeToFinishTurn) return;
			}
		}

		private Direction[] CalculateRepulsionForce(Loc ant)
		{
			// ������������� �� ����� ������������ � ����� ���������
			var force = State.MyHills.AttractionR2Force(ant, -3, State);
			force = force.Add(State.MyAnts.AttractionR2Force(ant, -1, State));
			V.MarkForceDirection(ant, force, 0xffffff80);
//			State.L("Force: " + force);
			return force.ToDirections();
		}

		private void RunInDirection(Loc ant, Direction[] directions, HashSet<Loc> antsBusyInThisTurn)
		{
			foreach (
				Direction dir in
					directions.Concat(new[] {Direction.East, Direction.North, Direction.South, Direction.West}.Shuffle(State.R)))
			{
				if (State.IsUnoccupied(ant, dir))
				{
					State.IssueOrder(ant, dir, antsBusyInThisTurn);
					break;
				}
			}
		}
	}
}