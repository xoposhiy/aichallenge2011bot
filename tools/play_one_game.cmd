@echo off
echo usage: play_one_game TURNS_COUNT [MAP]
del log*.txt
del debug.log
SET TURNS=%1
SET MAP=%2

if "%TURNS%" == "" (
    set TURNS=100
)

if "%MAP%" == "" (
	rem set MAP=maps\maze\maze_04p_01.map
	rem set MAP=maps\multi_hill_maze\maze_04p_02.map
	set MAP=maps\random_walk\random_walk_02p_02.map
	rem set MAP=maps\random_walk\random_walk_04p_02.map
)

python "%~dp0playgame.py" --engine_seed 42 --player_seed 42 --end_wait=0.25 --verbose --log_dir game_logs --turns %TURNS% --map_file "%~dp0%MAP%" "Bot.exe" "BattleBot.exe"
REM "2KillBot.exe" "BotOldFood.exe"

echo usage: play_one_game TURNS_COUNT [MAP]
