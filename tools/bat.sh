echo Battles statistics.
echo [Count] [Number of envolved ants]
awk '/Battle.+vs.+/ { print $3+$5 }' log* | sort | uniq -c | sort -d -r